#pragma once

#include <windows.h>
class CSerial
{

public:
	CSerial(void);
	~CSerial(void);

	void CloseCom(void);
	//打开串口
	BOOL OpenSerialPort(int Port, UINT baud_rate, BYTE date_bits, BYTE stop_bit, BYTE parity = NOPARITY);

	//发送数据
	BOOL SendData(char* data, int len);
public:
	HANDLE m_hComm;
	bool bOpenCom;
};

