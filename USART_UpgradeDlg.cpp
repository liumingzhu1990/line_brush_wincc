﻿
// USART_UpgradeDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "USART_Upgrade.h"
#include "USART_UpgradeDlg.h"
#include "afxdialogex.h"
#include <iostream>
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
#define BACKGOUND1			RGB(0, 128, 128)

using namespace std;
// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CUSARTUpgradeDlg 对话框



CUSARTUpgradeDlg::CUSARTUpgradeDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_USART_UPGRADE_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}
CUSARTUpgradeDlg::~CUSARTUpgradeDlg()
{
#ifdef ONE_DIALOG
	if (m_page2 != NULL) {
		delete m_page2;
		m_page2 = NULL;
	}
#else
#ifdef TWO_DIALOG
	if (m_page2 != NULL) {
		delete m_page2;
		m_page2 = NULL;
	}
	if (m_page1 != NULL) {
		delete m_page1;
		m_page1 = NULL;
	}
#else
	if (m_page1 != NULL) {
		delete m_page1;
		m_page1 = NULL;
	}	
	if (m_page2 != NULL) {
		delete m_page2;
		m_page2 = NULL;
	}
	if (m_page3 != NULL) {
		delete m_page3;
		m_page3 = NULL;
	}
#endif
#endif
}

void CUSARTUpgradeDlg::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_TAB1,m_tab);
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CUSARTUpgradeDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &CUSARTUpgradeDlg::OnTcnSelchangeTab1)
END_MESSAGE_MAP()


// CUSARTUpgradeDlg 消息处理程序
//#include "afxdialogex.h"
BOOL CUSARTUpgradeDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu; 
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	 //TODO: 在此添加额外的初始化代码
	
	//AllocConsole();								// 开辟控制台
	//SetConsoleTitle(_T("Debug Dialog"));		// 设置控制台窗口标题
	//freopen("CONOUT$", "w", stdout);            // 重定向输出
	//cout << "*******************Debug*******************" << endl;
	// 固定屏幕
	::SetWindowLong(m_hWnd, GWL_STYLE, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
	SetBackgroundColor(BACKGOUND1);//设置显示框的颜色

	// 页签
	OnTabCtrInit();
	//(CButton*)GetDlgItem(IDC_BUT_SET_VER)->ShowWindow(SW_HIDE);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CUSARTUpgradeDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CUSARTUpgradeDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CUSARTUpgradeDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CUSARTUpgradeDlg::OnTabCtrInit()
{
#ifdef ONE_DIALOG
	//为Tab Control增加两个页面  
	m_tab.InsertItem(0, _T("串口升级"));
	m_page2 = new UsartWrite();
	m_page2->Create(IDD_DIALOG2, &m_tab);
	m_tab.SetCurSel(0);
	OnTabCtlSelect();
#else
#ifdef TWO_DIALOG
	//为Tab Control增加两个页面  
	m_tab.InsertItem(0, _T("加密"));
	m_tab.InsertItem(1, _T("线刷升级"));
	m_page2 = new UsartWrite();
	m_page1 = new Encode();
	m_page1->Create(IDD_DIALOG1, &m_tab);
	m_page2->Create(IDD_DIALOG2, &m_tab);
#else
		//为Tab Control增加两个页面  
	m_tab.InsertItem(0, _T("加密"));
	m_tab.InsertItem(1, _T("线刷升级"));
	m_tab.InsertItem(2, _T("配置SN"));
	m_page1 = new Encode();
	m_page2 = new UsartWrite();
	m_page3 = new ConfigSN();
	m_page1->Create(IDD_DIALOG1, &m_tab);
	m_page2->Create(IDD_DIALOG2, &m_tab);
	m_page3->Create(IDD_DIALOG3, &m_tab);
#endif
	//设定在Tab内显示的范围  
	CRect tabRect;
	m_tab.GetClientRect(tabRect);
	tabRect.left += 1;
	tabRect.right -= 1;
	tabRect.top += 22;
	tabRect.bottom -= 1;
#ifdef TWO_DIALOG
	m_page1->MoveWindow(&tabRect);
	m_page2->MoveWindow(&tabRect);
#else
	m_page1->MoveWindow(&tabRect);
	m_page2->MoveWindow(&tabRect);
	m_page3->MoveWindow(&tabRect);
#endif
	//显示初始页面  
	m_tab.SetCurSel(1);
	OnTabCtlSelect();
#endif
}

void CUSARTUpgradeDlg::OnTabCtlSelect()
{
#ifdef ONE_DIALOG
	m_page2->ShowWindow(SW_SHOW);
#else
#ifdef TWO_DIALOG
	switch (m_tab.GetCurSel())
	{
	case 0:
		m_page2->ShowWindow(SW_HIDE);
		m_page1->ShowWindow(SW_SHOW);
		break;
	case 1:
		m_page2->ShowWindow(SW_SHOW);
		m_page1->ShowWindow(SW_HIDE);
		break;
	default:break;
	}
#else
	switch (m_tab.GetCurSel())
	{
	case 0:
		m_page1->ShowWindow(SW_SHOW);
		m_page2->ShowWindow(SW_HIDE);
		m_page3->ShowWindow(SW_HIDE);
		break;
	case 1:
		m_page1->ShowWindow(SW_HIDE);
		m_page2->ShowWindow(SW_SHOW);
		m_page3->ShowWindow(SW_HIDE);
		break;
	case 2:
		m_page1->ShowWindow(SW_HIDE);
		m_page2->ShowWindow(SW_HIDE);
		m_page3->ShowWindow(SW_SHOW);
		break;
	default:break;
	}
#endif
#endif
}

void CUSARTUpgradeDlg::OnTcnSelchangeTab1(NMHDR* pNMHDR, LRESULT* pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	*pResult = 0;
	OnTabCtlSelect();
}

void CUSARTUpgradeDlg::initControlFontSize()
{
	mySetFontSize(IDC_TAB1, 3.8f);	// 串口打开
}
bool CUSARTUpgradeDlg::mySetFontSize(int ID, float font_size)
{
	LOGFONT   logfont;//最好弄成类成员,全局变量,静态成员  
	CFont* pfont1 = GetDlgItem(ID)->GetFont();
	pfont1->GetLogFont(&logfont);
	logfont.lfHeight = (LONG)(logfont.lfHeight * font_size);   //这里可以修改字体的高比例
	logfont.lfWidth = (LONG)(logfont.lfWidth * font_size);   //这里可以修改字体的宽比例
	static   CFont   font1;
	font1.CreateFontIndirect(&logfont);
	GetDlgItem(ID)->SetFont(&font1);
	font1.Detach();

	return TRUE;
}


BOOL CAboutDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}
