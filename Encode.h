﻿#pragma once


// Encode 对话框

class Encode : public CDialogEx
{
	DECLARE_DYNAMIC(Encode)

public:
	Encode(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~Encode();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG1 };
#endif

private:
	CMFCEditBrowseCtrl m_EditBrowse1;
	int cnt1;
	int cnt2;
	int cnt3;
	int cnt4;
	int cnt5;
	int cnt11;

	CSpinButtonCtrl m_Spin1;
	CSpinButtonCtrl m_Spin2;
	CSpinButtonCtrl m_Spin3;
	CSpinButtonCtrl m_Spin4;
	CSpinButtonCtrl m_Spin5;
	CSpinButtonCtrl m_Spin11;
private:
	void initControlFontSize();				// 初始化控件字体
	bool mySetFontSize(int ID, float times);// 设置自己的字体 
	void initDlgContent();						// 初始化控件内容
	int Encode_Hex2Bin(CString fileName, unsigned char* data);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedEncodebtn();
	virtual BOOL OnInitDialog();
//	virtual void OnSetFont(CFont* pFont);
	afx_msg void OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin2(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin3(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin4(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin5(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin11(NMHDR* pNMHDR, LRESULT* pResult);
};
