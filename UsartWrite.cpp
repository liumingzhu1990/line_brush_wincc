﻿// UsartWrite.cpp: 实现文件
//

#include "pch.h"
#include "USART_Upgrade.h"
#include "UsartWrite.h"
#include "afxdialogex.h"
#include <iostream>
#include <string.h>
#include "CSerial.h"
#include <stdio.h>

using namespace std;
#define BACKGOUND3			RGB(51, 153, 155)
//#define BUFFER_SIZE			4096 // 13312 //8192
typedef struct versionNum {
	int m;	// 主版本
	int s;	// 子版本
	int r;	// 修订
}VersionNum;


int m_devtype = VCI_USBCAN2;
int m_devind = 0;
int m_CANInd = 0;
bool g_can_is_open;	// true打开；false关闭

/**********全局变量**************/
Serial_Data _g_serial_data = { 0 };
CSerial mCommUtils;

/**********函数声明***************/
VersionNum Get_Version_Num(char* version);
char* Rm_Version_Bracket(char* version);


IMPLEMENT_DYNAMIC(UsartWrite, CDialogEx)

UsartWrite::UsartWrite(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG2, pParent)
{
	m_serial_btn_sw = false;
	thread_rx_stopped = false;
	thread_tx_stopped = false;
	g_send_data_info.mode = No_Status;
	g_send_data_info.wait_time = 0;
	g_send_data_info.ver_Compatibility_flag = 0;
	g_send_data_info.ver_sw_flag = 0;

	m_devtype = VCI_USBCAN2;
	m_devind = 0;
	m_CANInd = 0;

	m_thread_recv_is_stop = false;
	m_thread_process_is_stop = false;
	m_thread_send_pkg_is_stop = false;
	g_can_is_open = false;

	g_send_data_info.is_dwnloading = false;	
}

UsartWrite::~UsartWrite()
{
	// 关闭串口线程
	if (m_serial_btn_sw) {
		m_serial_btn_sw = false;
		mCommUtils.CloseCom();

		thread_tx_stopped = false;
		thread_rx_stopped = false;
	}

	// 关闭CAN线程
	if (g_can_is_open) VCI_CloseDevice(m_devtype, m_devind);
	g_can_is_open = false;
	m_thread_recv_is_stop = false;
	m_thread_process_is_stop = false;
	m_thread_send_pkg_is_stop = false;
}

void UsartWrite::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_MFCEDITBROWSE2, m_EditBrowse);
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUT_OpenSerial, m_btn_serial_sw_handle);
	DDX_Control(pDX, IDC_BUT_SerialDown, m_btn_dwn_sw_handle);
	DDX_Control(pDX, IDC_BUT_READ_VER, m_btn_get_version_handle);
}


BEGIN_MESSAGE_MAP(UsartWrite, CDialogEx)
	ON_BN_CLICKED(IDC_BUT_OpenSerial, &UsartWrite::OnBnClickedButOpenserial)
	ON_CBN_SELCHANGE(IDC_COMBO1, &UsartWrite::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUT_SerialDown, &UsartWrite::OnBnClickedButSerialdown)
	ON_BN_CLICKED(IDC_BUT_SET_VER, &UsartWrite::OnBnClickedButSetVer)
	ON_BN_CLICKED(IDC_BUT_READ_VER, &UsartWrite::OnBnClickedButReadVer)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_10, &UsartWrite::OnDeltaposSpin10)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_6, &UsartWrite::OnDeltaposSpin6)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_7, &UsartWrite::OnDeltaposSpin7)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_8, &UsartWrite::OnDeltaposSpin8)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_9, &UsartWrite::OnDeltaposSpin9)
	ON_BN_CLICKED(IDC_Fresh_COM, &UsartWrite::OnBnClickedFreshCom)
	ON_WM_TIMER()
	ON_EN_CHANGE(IDC_MFCEDITBROWSE2, &UsartWrite::OnEnChangeMfceditbrowse2)
END_MESSAGE_MAP()


// UsartWrite 消息处理程序


BOOL UsartWrite::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	SetBackgroundColor(BACKGOUND3);//设置显示框的颜色
	SeekSerial();

	m_EditBrowse.EnableFileBrowseButton(_T(""), _T("Bin Files(*.bin)|*.bin|All Files (*.*)|*.*||"));
	initControlFontSize();
	initDlgContent();
	
	// CAN驱动初始化
	//Init_CAN_Module();

	
	return TRUE;  // return TRUE unless you set the focus to a control
}


int UsartWrite::SeekSerial()
{
	((CComboBox*)GetDlgItem(IDC_COMBO1))->ResetContent();
	HKEY hKey;
	if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		_T("HARDWARE\\DEVICEMAP\\SERIALCOMM"),	// HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM
		NULL,
		KEY_READ,
		&hKey) == ERROR_SUCCESS)				// 打开串口注册表对应的键值 
	{
		int i = 0;
		TCHAR portName[256], commName[256];
		DWORD dwLong, dwSize;
		while (1)
		{
			dwLong = dwSize = sizeof(portName);
			if (::RegEnumValue(hKey,
				i,
				portName,
				&dwLong,
				NULL,
				NULL,
				(PUCHAR)commName,
				&dwSize) == ERROR_NO_MORE_ITEMS)//   枚举串口  
				break;
			//printf("serial:%s\n", commName);
			((CComboBox*)GetDlgItem(IDC_COMBO1))->AddString(commName);
			((CComboBox*)GetDlgItem(IDC_COMBO1))->SetCurSel(0);
			i++;
		}
		RegCloseKey(hKey);
	}

	return 0;
}

bool start_CAN()
{
	VCI_INIT_CONFIG init_config;

	init_config.AccCode = 0;			// 验收码
	init_config.AccMask = 0xFFFFFFFF;	// 屏蔽码
	init_config.Filter = 0;				// 滤波方式
	init_config.Mode = 0;				// 工作模式
	init_config.Timing0 = 0x00;	// 定时器0
	init_config.Timing1 = 0x1C;	// 定时器1
	///* 设置波特率 */
	//switch (BaudRat){
	//	case BR_10K:		// 10Kbps
	//	{
	//		init_config.Timing0 = 0x31;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_20K:		// 20Kbps
	//	{
	//		init_config.Timing0 = 0x18;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_50K:		// 50Kbps
	//	{
	//		init_config.Timing0 = 0x09;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_125K:		// 125Kbps
	//	{
	//		init_config.Timing0 = 0x03;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_250K:		// 250Kbps
	//	{
	//		init_config.Timing0 = 0x01;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_500K:		// 500Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x1C;	// 定时器1
	//	}break;
	//	case BR_800K:		// 800Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x16;	// 定时器1
	//	}break;
	//	case BR_1000K:		// 1000Kbps
	//	{
	//		init_config.Timing0 = 0x00;	// 定时器0
	//		init_config.Timing1 = 0x14;	// 定时器1
	//	}break;
	//	default:break;
	//}

	/* 打开USBCAN设备 */
	if (VCI_OpenDevice(m_devtype, m_devind, 5) != STATUS_OK) // 500Kbps
	{
		printf("打开设备失败!\n");
		g_can_is_open = false;
		return false;
	}
	else
	{
		printf("打开USBCAN设备成功\n");
	}

	/* 初始化USBCAN设备 */
	if (VCI_InitCAN(m_devtype, m_devind, m_CANInd, &init_config) != STATUS_OK) {
		printf("初始化CAN失败!\n");
		VCI_CloseDevice(m_devtype, m_devind);
		g_can_is_open = false;
		return false;
	}
	else {
		printf("初始化USBCAN设备成功!\n");
	}

	if (VCI_StartCAN(m_devtype, m_devind, m_CANInd) == 1) {
		printf("CAN启动成功\n");
		g_can_is_open = true;
	}
	else {
		printf("CAN启动失败\n");
		g_can_is_open = false;
		return false;
	}

	return true;
}

void UsartWrite::Init_CAN_Module()
{
	// 启动CAN
	if (start_CAN()==false) {
		ToShowInfo("CAN启动失败。");
		return;
	}

	// 启用CAN接收线程
	m_thread_recv_is_stop = false;
	AfxBeginThread(Thread_Can_Recv, this);


	// 主动发送数据
	SetTimer(2, 2000, NULL);
}
void UsartWrite::OnBnClickedButOpenserial()
{
	if (m_serial_btn_sw == true) {
		m_serial_btn_sw = false;
		mCommUtils.CloseCom();
		mCommUtils.bOpenCom = false;
		g_send_data_info.ver_Compatibility_flag = 0;

		/* 结束接收线程 */
		thread_rx_stopped = true;
		//if (m_hThread_recv != INVALID_HANDLE_VALUE)
		//{
		//	/** 等待线程退出 */
		//	thread_rx_stopped = true;
		//	Sleep(10);
		//	/** 置线程句柄无效 */
		//	CloseHandle(m_hThread_recv);
		//	m_hThread_recv = INVALID_HANDLE_VALUE;
		//}
		// 关闭CAN
		//VCI_CloseDevice(m_devtype, m_devind);

		GetDlgItem(IDC_BUT_OpenSerial)->SetWindowTextA(_T("打开"));
		//GetDlgItem(IDC_BUT_SerialDown)->EnableWindow(false);
		ToShowInfo("Serial 关闭");

		KillTimer(1);
		return;
	}
	else {
		// 重新打开，先清空数据
		int nIndex = ((CComboBox*)GetDlgItem(IDC_COMBO1))->GetCurSel();
		CString strCBText;
		((CComboBox*)GetDlgItem(IDC_COMBO1))->GetLBText(nIndex, strCBText);
		char* com = strCBText.GetBuffer(0);
		char* pos = com + 3;
		comId = atoi(pos);

		//printf("%d\n", comId);
		if (mCommUtils.OpenSerialPort(comId, SERIAL_BAUD_115200,8,1) == false) {	
			ToShowInfo("Serial 打开失败。");
			return;
		}
		m_serial_btn_sw = true;
		GetDlgItem(IDC_BUT_OpenSerial)->SetWindowTextA(_T("关闭"));
		//GetDlgItem(IDC_BUT_SerialDown)->EnableWindow(true);

		//启动线程都串口
		thread_rx_stopped = false;
		AfxBeginThread(Thread_Serial_Recv, this);

		ToShowInfo("Serial 打开");
	}
}

void UsartWrite::Analysis_Recv_Data(char* data)
{
	if (strlen(data) < 4) return;
	/*
	串口升级过程中有2中模式：指令模式和用户升级模式
	在指令模式下：仅接收指令
	在用户升级模式下：仅接收升级包
	*/
	char* strx = NULL;
	//if (strstr(data, "Success")) {
	//	ToShowInfo("版本设置成功");
	//}
	//else 
	if ((strx = strstr(data, "RUN_V"))) {
		printf("%s\n", strx);
		CString str = strx;
		GetDlgItem(IDC_STA_READ_VER)->SetWindowTextA(str);
		str.ReleaseBuffer();
	}

	if (strstr(data, "OKOK")) {				// 发送下一包
		if (g_send_data_info.send_status == 2) {
			g_send_data_info.send_status = 0;
			ToShowInfo("升级成功，可选择断电重启设备。");

			// 使能获取版本号和关闭串口按钮
			m_btn_serial_sw_handle.EnableWindow(TRUE);
			m_btn_get_version_handle.EnableWindow(TRUE);
			// 下载完成
			g_send_data_info.is_dwnloading = false;

			thread_tx_stopped = true;		// 终止发送线程
			//Sleep(10);
			//CloseHandle(m_hThread_send);	// 关闭线程
		}
		else {
#if 1			// 若检测到对应包ID号，就直接下发对应的包ID；否则，从第一包开始下发
			char* id_str = NULL;
			char m_pkg_id[5] = { 0 };
			if ((id_str = strstr(data, "OK[")) != NULL) {
				//printf("%s\n", id_str);
				for (uint8_t i = 0; i < strlen(id_str + 3); i++) {
					//printf("%d:%c\n",i, id_str[i+3]);
					if (id_str[3 + i] == ']') {
						m_pkg_id[i] = '\0';
						break;
					}
					else {
						m_pkg_id[i] = id_str[3 + i];
					}
				}
				g_send_data_info.send_cnt = atoi(m_pkg_id);
				//printf("reply>pkg_id:%d\n", g_send_data_info.send_cnt);
			}
			else {
				//printf("pkg_id:%d\n", g_send_data_info.send_cnt);
				g_send_data_info.send_cnt++;
			}
#endif
			g_send_data_info.send_status = 1;
			//g_send_data_info.send_cnt++;
			g_send_data_info.mode = SEND_DATA;
			KillTimer(1);

			if (g_send_data_info.send_cnt == g_send_data_info.times && g_send_data_info.remain == 0) {
				g_send_data_info.send_status = 0;
				// 若为4096倍数的整数包，需要将进度条补满
				int pos = (int)(g_send_data_info.send_cnt * 1.0 / g_send_data_info.times * 100);
				prog->SetPos(pos);
				ToShowInfo("升级成功，可选择断电重启设备。");
				
				// 使能获取版本号和关闭串口按钮
				m_btn_serial_sw_handle.EnableWindow(TRUE);
				m_btn_get_version_handle.EnableWindow(TRUE);
				// 下载完成
				g_send_data_info.is_dwnloading = false;

				thread_tx_stopped = true;		// 终止发送线程
				//Sleep(10);
				//CloseHandle(m_hThread_send);	// 关闭线程
				g_send_data_info.mode = No_Status;
			}
		}
	}
	else if (strstr(data, "ERRERR")) {	// 重发
#if 1		// 若检测到对应包ID号，就直接下发对应的包ID；否则，从第一包开始下发
		char *id_str = NULL;
		char m_pkg_id[5] = { 0 };
		if ((id_str = strstr(data, "ERR[")) != NULL) {
			for (uint8_t i = 0; i < strlen(id_str + 4); i++) {
				//printf("%d:%c\n", i, id_str[i + 4]);
				if (id_str[4 + i] == ']') {
					m_pkg_id[i] = '\0';
					break;
				}
				else {
					m_pkg_id[i] = id_str[4 + i];
				}
			}
			g_send_data_info.send_cnt = atoi(m_pkg_id);
			//printf("reply>pkg_id:%d\n", g_send_data_info.send_cnt);
		}
		else {
			g_send_data_info.send_cnt = 0;
		}
#else
		g_send_data_info.send_cnt = 0;
#endif
		g_send_data_info.send_status = 1;
		g_send_data_info.mode = SEND_DATA;

		//g_send_data_info.times = g_send_data_info.len / PKG_DATA_SIZE;
		//g_send_data_info.remain = g_send_data_info.len % PKG_DATA_SIZE;
		//g_send_data_info.send_cnt = 0;

		//ToShowInfo("[Recv]发送中遇到错误，开始重传...");
	}
	else if (strstr(data, "---Run_V-")) {	// 应用程序运行后，提示用语	
		ToShowInfo("应用程序已运行...");
		// 使能 打开 和 获取版本号 按钮
		m_btn_serial_sw_handle.EnableWindow(TRUE);
		m_btn_get_version_handle.EnableWindow(TRUE);
		// 下载异常
		g_send_data_info.is_dwnloading = false;

		// 进度条设置成0%
		prog->SetPos(0);
	}
	else if (strstr(data, "1111")) {	// 发送信息，反馈成功
		//g_send_data_info.low_baud_flag = 0;
		 //g_send_data_info.ver_sw_flag = 0;
		 KillTimer(1);
		//反馈成功
		if (g_send_data_info.recv_status == 1) {
			g_send_data_info.recv_status = 0;
			g_send_data_info.send_status = 1;
			g_send_data_info.mode = SEND_DATA;
			ToShowInfo("发送升级包中...");
		}
		else g_send_data_info.mode = No_Status;
	}
	else if (strstr(data, "2222")) {	// 发送信息，反馈失败
		ToShowInfo("通讯异常，请重新下载，或版本不兼容。");
		// 反馈失败
		g_send_data_info.mode = No_Status;

		// 使能获取版本号和关闭串口按钮
		m_btn_serial_sw_handle.EnableWindow(TRUE);
		m_btn_get_version_handle.EnableWindow(TRUE);

		// 下载异常
		g_send_data_info.is_dwnloading = false;
	}
}

//UINT WINAPI UsartWrite::Thread_Serial_Recv(void* pParam)
UINT UsartWrite::Thread_Serial_Recv(void* pParam)
{
	/** 得到本类的指针 */
	UsartWrite* pThread = reinterpret_cast<UsartWrite*>(pParam);

	if (!pThread->m_serial_btn_sw) {
		return 1;
	}

	PurgeComm(mCommUtils.m_hComm, PURGE_RXCLEAR | PURGE_TXCLEAR);	//清空串口

	//printf("start recv thread.\n");
	while (!pThread->thread_rx_stopped)
	{
		if (mCommUtils.m_hComm == INVALID_HANDLE_VALUE) {
			printf("recv thread failed.\n");
			break;
		}

		BOOL bReadOK = ReadFile(mCommUtils.m_hComm, _g_serial_data.recv_data, DATA_LEN_1024, &_g_serial_data.recv_len, NULL);
		
		if (bReadOK && (_g_serial_data.recv_len > 0)) {
			if (_g_serial_data.recv_data[_g_serial_data.recv_len - 1] != '\n') {
				strcat(_g_serial_data.buff_data + strlen(_g_serial_data.buff_data), _g_serial_data.recv_data);
				memset(_g_serial_data.recv_data, 0, sizeof(_g_serial_data.recv_data));
				continue;
			}
			else {
				strcat(_g_serial_data.buff_data + strlen(_g_serial_data.buff_data), _g_serial_data.recv_data);
				memset(_g_serial_data.recv_data, 0, sizeof(_g_serial_data.recv_data));

				memcpy(_g_serial_data.dispose_data, _g_serial_data.buff_data, strlen(_g_serial_data.buff_data));
				memset(_g_serial_data.buff_data, 0, sizeof(_g_serial_data.buff_data));
			}

			//清空串口
			PurgeComm(mCommUtils.m_hComm, PURGE_RXCLEAR | PURGE_TXCLEAR);

			printf("%s", _g_serial_data.dispose_data);
			pThread->Analysis_Recv_Data(_g_serial_data.dispose_data);
			memset(_g_serial_data.dispose_data, 0, sizeof(_g_serial_data.dispose_data));
		}
	}
	//printf("end recv thread.\n");

	//_endthreadex(0);	// stop _beginThreadex();
		
	return 0;
}


// V1.0.0.
VersionNum Get_Version_Num(char* version_s)
{
	VersionNum ver_t ;
	char version[50] = "";
	strcpy(version, version_s+1);
	char* p = strtok(version, ".");	// 去掉V
	int num = 0;
	while (p) {
		switch (num) {
		case 0:ver_t.m = atoi(p); break;
		case 1:ver_t.s = atoi(p); break;
		case 2:ver_t.r = atoi(p); break;
		}

		p = strtok(NULL, ".");
		num++;
	}
	//printf("%d.%d.%d\n",ver_t.m, ver_t.s,ver_t.r);
	return ver_t;
}
// 去版本号中的括号
char *Rm_Version_Bracket(char *version)
{
	char* tmp = (char*)malloc(strlen(version));
	memset(tmp, 0, strlen(version));
	int cnt = 0;
	// 默认括号是成对出现的，而且是先是'(',后是')'；
	for (int i = 0; i < (int)strlen(version);i++) {
		if (version[i] == '(') {
			char* p = strtok(version, "(");
			tmp = p;
			//printf(">%s,%s\n", version, tmp);
			strcat(tmp, ".bin");
			return tmp;
		}
	}
	tmp = version;
	//printf(">>%s,%s\n",version,tmp);
	return tmp;
}
//UINT WINAPI UsartWrite::Thread_Serial_Send(void* pParam)
UINT UsartWrite::Thread_Serial_Send(void* pParam)
{
	//printf("start send thread.\n");

	/** 得到本类的指针 */
	UsartWrite* pthread = reinterpret_cast<UsartWrite*>(pParam);

	unsigned char* data = NULL;
	if (!pthread->m_serial_btn_sw) {
		if (pthread->g_send_data_info.data) free(pthread->g_send_data_info.data);
		if (pthread->g_send_data_info.sub_data) free(pthread->g_send_data_info.sub_data);
		return 1;
	}
	else {
		//g_send_data_info.data = (unsigned char*)calloc(g_send_data_info.len, 1);
		pthread->g_send_data_info.sub_data = (unsigned char*)calloc(PKG_SUB_SIZE, 1);
		pthread->g_send_data_info.len = 0;
	}

	// 读取文件大小
	CString fileName("");
	CString fileName_t("");
	pthread->m_EditBrowse.GetWindowTextA(fileName);
	if (fileName.GetLength() < 5) {
		// 使能 打开 和 获取版本号 按钮
		pthread->m_btn_serial_sw_handle.EnableWindow(TRUE);
		pthread->m_btn_get_version_handle.EnableWindow(TRUE);
		pthread->ToShowInfo("请先加载升级包~");
		// 下载异常
		pthread->g_send_data_info.is_dwnloading = false;
		return 1;
	}
	if (!mCommUtils.bOpenCom) {
		// 使能 打开 和 获取版本号 按钮
		pthread->m_btn_serial_sw_handle.EnableWindow(TRUE);
		pthread->m_btn_get_version_handle.EnableWindow(TRUE);
		pthread->ToShowInfo("请先打开串口~");
		// 下载异常
		pthread->g_send_data_info.is_dwnloading = false;
		return 1;
	}
	fileName_t = fileName;
	//获取文件名称
	//char version_t[DATA_LEN_50] = { 0 };
	char* p_ver = strtok(fileName.GetBuffer(), "\\");
	fileName.ReleaseBuffer();

	while (p_ver) {
		strcpy(pthread->g_send_data_info.version, p_ver);
		p_ver = strtok(NULL, "\\");
	}
	p_ver = NULL;

	pthread->g_send_data_info.version[strlen(pthread->g_send_data_info.version) - 4] = '\0';
	printf("[Line APP]version:%s\n", pthread->g_send_data_info.version);

	// 避免获取的文件中含所有括号，例如 V1.0.0.L1C1(1).bin，若存储需要先将其去除
	strcpy(pthread->g_send_data_info.version, Rm_Version_Bracket(pthread->g_send_data_info.version));

	// 修改原因，实现强制刷机，add lmz 20220720
	//// 添加版本判断
	//if (run_ver.empty()) {
	//	ToShowInfo("请先获取版本号。");
	//	return;
	//}
	//VersionNum runV = Get_Version_Num((char *)run_ver.c_str());
	//VersionNum upV = Get_Version_Num(g_send_data_info.version);
	//if (runV.m < upV.m || runV.s < upV.s || runV.r < upV.r) {
	//}
	//else {
	//	ToShowInfo("升级版本太低，无需升级。");
	//	return;
	//}
	// 获取文件内容
	FILE* pFile;
	_wfopen_s(&pFile, fileName_t.AllocSysString(), L"rb"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
	if (pFile == NULL) //判断文件是否打开成功
	{
		pthread->MessageBox("fail to open", "warning", MB_ICONSTOP);
		return 1;
	}

	pthread->g_send_data_info.len = 0;
	fseek(pFile, 0, SEEK_END);//将文件指针设置到文件末尾处
	pthread->g_send_data_info.len = ftell(pFile);//获取文件指针的位置 也就相当于文件的大小了
	fseek(pFile, 0, SEEK_SET);//重新将文件指针调回文件开头

	pthread->g_send_data_info.data = (unsigned char*)calloc(pthread->g_send_data_info.len, 1);

	fread(pthread->g_send_data_info.data, 1, pthread->g_send_data_info.len, pFile);//将整个文件读取 注意这里文件的大小不应超过65536
	fclose(pFile);//关闭文件

	// 计算文件匹配，否则提示 bin_type
	if (pthread->g_send_data_info.bin_type == 1) {	// bootloader的bin升级包
		if (pthread->g_send_data_info.len > 1024 * 64) {
			pthread->ToShowInfo("请加载Bootloader的Bin升级包。");
			// 使能 打开 和 获取版本号 按钮
			pthread->m_btn_serial_sw_handle.EnableWindow(TRUE);
			pthread->m_btn_get_version_handle.EnableWindow(TRUE);
			// 下载异常
			pthread->g_send_data_info.is_dwnloading = false;
			return 1;
		}
	}
	else if (pthread->g_send_data_info.bin_type == 0) {
		if (pthread->g_send_data_info.len < 1024 * 64) {
			pthread->ToShowInfo("请加载Application的Bin升级包。");
			// 使能 打开 和 获取版本号 按钮
			pthread->m_btn_serial_sw_handle.EnableWindow(TRUE);
			pthread->m_btn_get_version_handle.EnableWindow(TRUE);
			// 下载异常
			pthread->g_send_data_info.is_dwnloading = false;
			return 1;
		}
	}

	// 计算耗时
	pthread->g_send_data_info.time = (int)((pthread->g_send_data_info.len - 20) / 4096.0 * 0.2836);	// baud:115200,use time ,lmz update 20220720

	char m_use_ime[DATA_LEN_50] = "";
	sprintf(m_use_ime, "预计耗时%d秒", pthread->g_send_data_info.time);
	pthread->ToShowInfo("");
	pthread->ToShowInfo(m_use_ime);

	pthread->g_send_data_info.checkout = 0;

	// 是否是有效升级包
	if (pthread->g_send_data_info.data[0] == 0x00 && pthread->g_send_data_info.data[1] == 0x11 && pthread->g_send_data_info.data[2] == 0x22 && \
		pthread->g_send_data_info.data[pthread->g_send_data_info.len - 3] == 0xDD && pthread->g_send_data_info.data[pthread->g_send_data_info.len - 2] == 0xEE \
		&& pthread->g_send_data_info.data[pthread->g_send_data_info.len - 1] == 0xFF)
	{
		if (pthread->g_send_data_info.ver_Compatibility_flag == 1) {
			sprintf(pthread->g_send_data_info.cmd, "<ZKHYSET*PACKAGESIZE,%s,%d>\r\n", \
				pthread->g_send_data_info.version, (int)(pthread->g_send_data_info.len - 20));
		}
		else {
			sprintf(pthread->g_send_data_info.cmd, "<ZKHYSET*PKGINFO,%s,%d,%d>\r\n", \
				pthread->g_send_data_info.version, (int)(pthread->g_send_data_info.len - 20), pthread->g_send_data_info.bin_type);
		}
		//printf("%s\n", g_send_data_info.cmd);
		pthread->g_send_data_info.mode = SEND_CMD;
		// 取出有效的Bin包
		data = pthread->g_send_data_info.data + 8;
		pthread->g_send_data_info.len -= 20;
		pthread->g_send_data_info.times = pthread->g_send_data_info.len / PKG_DATA_SIZE;
		pthread->g_send_data_info.remain = pthread->g_send_data_info.len % PKG_DATA_SIZE;
		pthread->g_send_data_info.send_cnt = 0;

		printf("times:%d,remain:%d\n", pthread->g_send_data_info.times, pthread->g_send_data_info.remain);

		pthread->g_send_data_info.is_dwnloading = true;	// 下载中
		uint8_t loop_times = 0;
		while (!pthread->thread_tx_stopped)
		{
			switch (pthread->g_send_data_info.mode) {
			case No_Status: {		// 等待				
				Sleep(20);
				// 添加超时处理，8秒没有回复就认为通讯异常（下载失败）
				if (loop_times >= 400) {	// 8秒超时异常判断
					loop_times = 0;
					pthread->ToShowInfo("下载超时[8秒]已主动断开连接，请先重启设备后再执行线刷操作。");
					goto TO_SERIAL_SEND_END;
				}
				else loop_times++;
			}break;
			case SEND_CMD: {		// 发送信息
				pthread->g_send_data_info.mode = No_Status;
				pthread->g_send_data_info.recv_status = 1;
				printf("ver cmd:%s\n", pthread->g_send_data_info.cmd);
				if (mCommUtils.bOpenCom) {
					mCommUtils.SendData(pthread->g_send_data_info.cmd, (int)strlen(pthread->g_send_data_info.cmd));
				}
				else {
					pthread->ToShowInfo("串口已经断开连接");
					goto TO_SERIAL_SEND_END;
					/* 发送线程结束 */
					//if (pthread->m_hThread_send != INVALID_HANDLE_VALUE)
					//{
					//	/** 等待线程退出 */
					//	Sleep(10);
					//	/** 置线程句柄无效 */
					//	CloseHandle(pthread->m_hThread_send);
					//}
					//return 1;
				}
				pthread->g_send_data_info.send_cnt = 0;
				pthread->g_send_data_info.send_status = 0;
				pthread->ToShowInfo("发送版本信息");
				pthread->g_send_data_info.ver_sw_flag = !pthread->g_send_data_info.ver_sw_flag;
				loop_times = 0;
				// 开定时器，判断是否接收反馈信息，为了兼容
				pthread->SetTimer(1, 4000, NULL);
			}break;
			case SEND_DATA: {			// 发送升级包数据
				// 分包验证 并 下发
				pthread->g_send_data_info.mode = No_Status;

				if (pthread->g_send_data_info.send_status == 1) {
					pthread->g_send_data_info.send_status = 0;

					if (pthread->g_send_data_info.send_cnt < pthread->g_send_data_info.times) {	// 整包
						// 数据拷贝
						memcpy(pthread->g_send_data_info.sub_data, data + (pthread->g_send_data_info.send_cnt * PKG_DATA_SIZE), PKG_DATA_SIZE);

						// 求校验值
						pthread->g_send_data_info.checkout = 0;
						for (int i = 0; i < PKG_DATA_SIZE; i++)
						{
							pthread->g_send_data_info.checkout += pthread->g_send_data_info.sub_data[i];
						}
						pthread->g_send_data_info.sub_data[PKG_DATA_SIZE] = pthread->g_send_data_info.checkout & 0xFF;
						pthread->g_send_data_info.sub_data[PKG_DATA_SIZE + 1] = (pthread->g_send_data_info.checkout >> 8) & 0xFF;
						pthread->g_send_data_info.sub_data[PKG_DATA_SIZE + 2] = (pthread->g_send_data_info.checkout >> 16) & 0xFF;
						pthread->g_send_data_info.sub_data[PKG_DATA_SIZE + 3] = (pthread->g_send_data_info.checkout >> 24) & 0xFF;
						pthread->g_send_data_info.sub_data[PKG_DATA_SIZE + 4] = (pthread->g_send_data_info.send_cnt + 1);

						//for (int ij = 4; ij >= 0; ij--) {
						//	printf("%02X ", pthread->g_send_data_info.sub_data[PKG_DATA_SIZE + ij]);
						//}
						//printf("\n");

						// 发送数据
						if (!mCommUtils.SendData((char*)pthread->g_send_data_info.sub_data, PKG_SUB_SIZE)) {
							printf("send %d error.\n", pthread->g_send_data_info.send_cnt + 1);
							pthread->g_send_data_info.send_cnt = 0;
							pthread->g_send_data_info.send_status = 1;
							pthread->ToShowInfo("[Send1]发送中遇到错误，开始重传...");
							pthread->g_send_data_info.mode = SEND_DATA;
						}
					}
					else if (pthread->g_send_data_info.remain > 0) {		// 余数包

						// 数据拷贝
						memcpy(pthread->g_send_data_info.sub_data, data + (pthread->g_send_data_info.send_cnt * PKG_DATA_SIZE), pthread->g_send_data_info.remain);

						// 求校验值
						pthread->g_send_data_info.checkout = 0;
						for (int i = 0; i < pthread->g_send_data_info.remain; i++)
						{
							pthread->g_send_data_info.checkout += pthread->g_send_data_info.sub_data[i];
						}

						pthread->g_send_data_info.sub_data[pthread->g_send_data_info.remain] = pthread->g_send_data_info.checkout & 0xFF;
						pthread->g_send_data_info.sub_data[pthread->g_send_data_info.remain + 1] = (pthread->g_send_data_info.checkout >> 8) & 0xFF;
						pthread->g_send_data_info.sub_data[pthread->g_send_data_info.remain + 2] = (pthread->g_send_data_info.checkout >> 16) & 0xFF;
						pthread->g_send_data_info.sub_data[pthread->g_send_data_info.remain + 3] = (pthread->g_send_data_info.checkout >> 24) & 0xFF;
						pthread->g_send_data_info.sub_data[pthread->g_send_data_info.remain + 4] = (pthread->g_send_data_info.send_cnt + 1);

						//for (int ij = 4; ij >= 0; ij--) {
						//	printf("%02X ", pthread->g_send_data_info.sub_data[pthread->g_send_data_info.remain + ij]);
						//}
						//printf("\n");
						// 发送数据，若失败，需要重传
						if (!mCommUtils.SendData((char*)pthread->g_send_data_info.sub_data, pthread->g_send_data_info.remain + 5)) {
							printf("send %d error.\n", pthread->g_send_data_info.send_cnt + 1);
							pthread->g_send_data_info.send_cnt = 0;
							pthread->g_send_data_info.send_status = 1;
							pthread->ToShowInfo("[Send2]发送中遇到错误，开始重传...");
							pthread->g_send_data_info.mode = SEND_DATA;
						}
						else {
							pthread->g_send_data_info.send_status = 2;
							pthread->g_send_data_info.remain = 0;	
						}
					}

					//pthread->g_send_data_info.checkout = 0;
					printf("\n--------%d--%d--%d------\n", pthread->g_send_data_info.send_cnt, pthread->g_send_data_info.times, pthread->g_send_data_info.remain);

					// 进度条显示
					int pos = (int)((pthread->g_send_data_info.send_cnt) * 1.0 / pthread->g_send_data_info.times * 100);
					memset(pthread->g_send_data_info.sub_data, 0, sizeof(pthread->g_send_data_info.sub_data));
					loop_times = 0;
					pthread->prog->SetPos(pos);

					continue;
				}
			}break;
			}
		}
	}
	else {
		pthread->ToShowInfo("不是有效的升级包~");
	}

TO_SERIAL_SEND_END:
	pthread->thread_tx_stopped = true;
	pthread->g_send_data_info.is_dwnloading = false;
	if (pthread->g_send_data_info.data) {
		free(pthread->g_send_data_info.data);
		pthread->g_send_data_info.data = NULL;
	}

	if (pthread->g_send_data_info.sub_data) {
		free(pthread->g_send_data_info.sub_data);
		pthread->g_send_data_info.sub_data = NULL;
	}

	// 使能获取版本号和关闭串口按钮
	pthread->m_btn_serial_sw_handle.EnableWindow(TRUE);
	pthread->m_btn_get_version_handle.EnableWindow(TRUE);

	//printf("end Send thread.\n");
	//_endthreadex(0);	// stop _beginThreadex();

	return 0;
}

void UsartWrite::OnCbnSelchangeCombo1()
{
	// TODO: 在此添加控件通知处理程序代码
	//SeekSerial();
}

void UsartWrite::ToShowInfo(char * data)
{
	CString str(data);
	((CListBox*)GetDlgItem(IDC_LIST1))->AddString(str);
	//((CListBox*)GetDlgItem(IDC_LIST1))->SetCurSel(listbox_cnt++);

	// 自动滚动
	((CListBox*)GetDlgItem(IDC_LIST1))->SetCurSel(((CListBox*)GetDlgItem(IDC_LIST1))->GetCount() - 1);

}

void UsartWrite::OnBnClickedButSerialdown()
{
	if (g_send_data_info.is_dwnloading == true) {
		ToShowInfo("....升级中...请勿打断...");
		return;
	}
	// 升级包的文件类型
	g_send_data_info.bin_type = ((CComboBox*)GetDlgItem(IDC_Bin_SWH))->GetCurSel();
	//printf("bin_type:%d\n", g_send_data_info.bin_type);

	/** 开启串口数据监听线程 */
	if (g_send_data_info.bin_type < 2) {	// 串口 升级 Applation   Bootloader
		// 判断串口是否开启
		if (m_serial_btn_sw) {
			thread_tx_stopped = false;
			AfxBeginThread(Thread_Serial_Send, this);
		}
		else {
			ToShowInfo("请先打开串口。");
			// 使能 打开 和 获取版本号 按钮
			m_btn_serial_sw_handle.EnableWindow(TRUE);
			m_btn_get_version_handle.EnableWindow(TRUE);
			// 下载异常
			g_send_data_info.is_dwnloading = false;
			return;
		}
	}
	else {	// CAN 升级--Screen
		// 将向加载升级包
		m_EditBrowse.GetWindowTextA(m_fileName);
		if (m_fileName.GetLength() < 5) {
			// 使能 打开 和 获取版本号 按钮
			m_btn_serial_sw_handle.EnableWindow(TRUE);
			m_btn_get_version_handle.EnableWindow(TRUE);
			ToShowInfo("请先加载升级包~");
			// 下载异常
			g_send_data_info.is_dwnloading = false;
			return ;
		}

		// 启动CAN
		if (g_can_is_open == false) {
			if (start_CAN() == false) {
				ToShowInfo("CAN启动失败。");
				// 使能 打开 和 获取版本号 按钮
				m_btn_serial_sw_handle.EnableWindow(TRUE);
				m_btn_get_version_handle.EnableWindow(TRUE);
				// 下载异常
				g_send_data_info.is_dwnloading = false;
				return;
			}
			else {
				// 启用CAN接收线程
				m_thread_recv_is_stop = false;
				m_thread_process_is_stop = false;
				AfxBeginThread(Thread_Can_Recv, this);
				AfxBeginThread(Thread_Can_Process, this);

				ToShowInfo("CAN 已启用");
				// 执行下载程序
				m_thread_send_pkg_is_stop = false;
				AfxBeginThread(Thread_Can_Upgrade_Logic, this);
			}
		}
	}
	// 启动成功后，关闭 打开 和 获取版本号 按钮
	m_btn_serial_sw_handle.EnableWindow(FALSE);
	m_btn_get_version_handle.EnableWindow(FALSE);

	// 下载初始化
	g_send_data_info.is_dwnloading = false;
}

void UsartWrite::initControlFontSize()
{
	mySetFontSize(IDC_BUT_OpenSerial, 1.8f);	// 串口打开
	mySetFontSize(IDC_BUT_SerialDown, 1.8f);	// 下载-烧写
	mySetFontSize(IDC_Fresh_COM, 1.5f);		// 刷新串口
	mySetFontSize(IDC_LIST1, 1.3f);			// 检索标题栏
	mySetFontSize(IDC_MFCEDITBROWSE2, 1.3f);	// 检索标题栏
	mySetFontSize(IDC_COMBO1, 1.8f);			// 串口下拉
	mySetFontSize(IDC_Bin_SWH, 1.8f);		// 下载bin包切换

	mySetFontSize(IDC_STA_READ_VER, 1.5f);	// 获取版本号
	mySetFontSize(IDC_BUT_SET_VER, 1.5f);	// 设置版本
	mySetFontSize(IDC_BUT_READ_VER, 1.5f);	// 获取版本
	mySetFontSize(IDC_COMBO_7, 1.8f);		// 下拉 V
	mySetFontSize(IDC_COMBO_5, 1.8f);		// 下拉 L S
	mySetFontSize(IDC_COMBO_6, 1.8f);		// 下拉 C
	mySetFontSize(IDC_EDIT_10, 1.8f);		// 主版本
	mySetFontSize(IDC_EDIT_6, 1.8f);			// 次版本
	mySetFontSize(IDC_EDIT_7, 1.8f);			// 修订版本
	mySetFontSize(IDC_EDIT_8, 1.8f);			// 几个相机
	mySetFontSize(IDC_EDIT_9, 1.8f);			// 几个超声波雷达
}
void UsartWrite::initDlgContent()
{
	((CComboBox*)GetDlgItem(IDC_COMBO_7))->AddString("V");
	((CComboBox*)GetDlgItem(IDC_COMBO_7))->SetCurSel(0);

	((CComboBox*)GetDlgItem(IDC_COMBO_5))->AddString("L");
	((CComboBox*)GetDlgItem(IDC_COMBO_5))->AddString("S");
	((CComboBox*)GetDlgItem(IDC_COMBO_5))->SetCurSel(0);

	((CComboBox*)GetDlgItem(IDC_COMBO_6))->AddString("C");
	((CComboBox*)GetDlgItem(IDC_COMBO_6))->SetCurSel(0);
	cnt1 = cnt2 = cnt4 = cnt5 = 1;
	cnt3 = 0;
	((CComboBox*)GetDlgItem(IDC_EDIT_10))->SetWindowTextA("1");
	((CComboBox*)GetDlgItem(IDC_EDIT_6))->SetWindowTextA("0");
	((CComboBox*)GetDlgItem(IDC_EDIT_7))->SetWindowTextA("0");
	((CComboBox*)GetDlgItem(IDC_EDIT_8))->SetWindowTextA("1");
	((CComboBox*)GetDlgItem(IDC_EDIT_9))->SetWindowTextA("1");
	// 获取版本号
	GetDlgItem(IDC_STA_READ_VER)->SetWindowTextA("显示版本号");

	// bin包切换控制
	TCHAR BinName[10] = { 0 };
	sprintf(BinName, "Application");
	((CComboBox*)GetDlgItem(IDC_Bin_SWH))->AddString(BinName);
	sprintf(BinName, "Bootloader");
	((CComboBox*)GetDlgItem(IDC_Bin_SWH))->AddString(BinName);
	sprintf(BinName, "Screen");
	((CComboBox*)GetDlgItem(IDC_Bin_SWH))->AddString(BinName);

	((CComboBox*)GetDlgItem(IDC_Bin_SWH))->SetCurSel(0);
	//int index = ((CComboBox*)GetDlgItem(IDC_Bin_SWH))->GetCurSel();
	//printf("index:%d\n", index);

	//((CComboBox*)GetDlgItem(IDC_Bin_SWH))->SetCurSel(1);
	//index = ((CComboBox*)GetDlgItem(IDC_Bin_SWH))->GetCurSel();
	//printf("index:%d\n", index);
	
	// 下载按钮变成灰色（不能按）
	//GetDlgItem(IDC_BUT_SerialDown)->EnableWindow(false);

	// 进度条
	prog = (CProgressCtrl*)GetDlgItem(IDC_PROGRESS1);
	prog->SetRange(0, 100);
	prog->SetPos(0);
}

bool UsartWrite::mySetFontSize(int ID, float times)
{
	LOGFONT   logfont;//最好弄成类成员,全局变量,静态成员  
	CFont* pfont1 = GetDlgItem(ID)->GetFont();
	pfont1->GetLogFont(&logfont);
	logfont.lfHeight = (LONG)(logfont.lfHeight * times);   //这里可以修改字体的高比例
	logfont.lfWidth = (LONG)(logfont.lfWidth * times);   //这里可以修改字体的宽比例
	static   CFont   font1;
	font1.CreateFontIndirect(&logfont);
	GetDlgItem(ID)->SetFont(&font1);
	font1.Detach();

	return TRUE;
}

void UsartWrite::OnBnClickedButSetVer()
{
	//if (!mCommUtils.bOpenCom) {
	//	ToShowInfo("请先打开串口~");
	//	return;
	//}
	//// 获取文件名称 V1.1.0.L1C6
	//char version[20] = { 0 };
	//CString Version(""), V(""), LS(""), C(""), M_v(""), S_v(""), R_v(""), LS_n(""), C_n("");
	//((CComboBox*)GetDlgItem(IDC_COMBO_7))->GetWindowTextA(V);
	//((CComboBox*)GetDlgItem(IDC_COMBO_5))->GetWindowTextA(LS);
	//((CComboBox*)GetDlgItem(IDC_COMBO_6))->GetWindowTextA(C);
	//((CEdit*)GetDlgItem(IDC_EDIT_10))->GetWindowTextA(M_v);
	//((CEdit*)GetDlgItem(IDC_EDIT_6))->GetWindowTextA(S_v);
	//((CEdit*)GetDlgItem(IDC_EDIT_7))->GetWindowTextA(R_v);
	//((CEdit*)GetDlgItem(IDC_EDIT_8))->GetWindowTextA(LS_n);
	//((CEdit*)GetDlgItem(IDC_EDIT_9))->GetWindowTextA(C_n);
	//Version = V + M_v + "." + S_v + "." + R_v + "." + LS + LS_n + C + C_n;
	//// 发送<ZKHYSET*VERSION:V1.0.0.S1C6,V1.0.0.S1C6>
	//char cmd[40] = "";
	//sprintf(cmd,"<ZKHYSET*RUNVERSION:%s>\r\n", Version);
	////printf("%s",cmd);
	//mCommUtils.SendData(cmd, strlen(cmd));
}


void UsartWrite::OnBnClickedButReadVer()
{
	if (!mCommUtils.bOpenCom) {
		ToShowInfo("请先打开串口~");
		return;
	}
	int bin_type = ((CComboBox*)GetDlgItem(IDC_Bin_SWH))->GetCurSel();
	//printf("bin_type:%d\n", bin_type);

	char cmd[40] = {0};
	// 发送<ZKHYCHK*VERSION>
	switch (bin_type) {
	case 0: {
		sprintf(cmd, "<ZKHYCHK*VERSION>\r\n");
	}break;
	case 1: {
		sprintf(cmd, "<ZKHYCHK*BOOTVERSION>\r\n");
	}break;
	}

	//printf("%s", cmd);
	mCommUtils.SendData(cmd, (int)strlen(cmd));
}


void UsartWrite::OnDeltaposSpin10(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt1++;
		if (cnt1 >= 100) cnt1 = 100;
	}
	else {							// -
		cnt1--;
		if (cnt1 < 0) cnt1 = 0;
	}
	m_editNum.Format("%d", cnt1);
	((CComboBox*)GetDlgItem(IDC_EDIT_10))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void UsartWrite::OnDeltaposSpin6(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt2++;
		if (cnt2 >= 100) cnt2 = 100;
	}
	else {							// -
		cnt2--;
		if (cnt2 < 0) cnt2 = 0;
	}
	m_editNum.Format("%d", cnt2);
	((CComboBox*)GetDlgItem(IDC_EDIT_6))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void UsartWrite::OnDeltaposSpin7(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt3++;
		if (cnt3 >= 100) cnt3 = 100;
	}
	else {							// -
		cnt3--;
		if (cnt3 < 0) cnt3 = 0;
	}
	m_editNum.Format("%d", cnt3);
	((CComboBox*)GetDlgItem(IDC_EDIT_7))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void UsartWrite::OnDeltaposSpin8(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt4++;
		if (cnt4 >= 100) cnt4 = 100;
	}
	else {							// -
		cnt4--;
		if (cnt4 < 0) cnt4 = 0;
	}
	m_editNum.Format("%d", cnt4);
	((CComboBox*)GetDlgItem(IDC_EDIT_8))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void UsartWrite::OnDeltaposSpin9(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt5++;
		if (cnt5 >= 100) cnt5 = 100;
	}
	else {							// -
		cnt5--;
		if (cnt5 < 0) cnt5 = 0;
	}
	m_editNum.Format("%d", cnt5);
	((CComboBox*)GetDlgItem(IDC_EDIT_9))->SetWindowTextA(m_editNum);
	*pResult = 0;
}



void UsartWrite::OnBnClickedFreshCom()
{
	// TODO: 在此添加控件通知处理程序代码
	SeekSerial();
}


void UsartWrite::OnTimer(UINT_PTR nIDEvent)
{
	switch (nIDEvent) {
	case 0: {	// CAN发送数据超时操作
		//Can_Send_Data(m_can_send_pbj);
		if (g_can_is_open == true) 
			CANSendData(m_can_send_pbj.ID, m_can_send_pbj.DataLen, m_can_send_pbj.Data, DataFrame, StandardFrame, NormalSend);
	}break;
	case 1: {
		if (g_send_data_info.ver_sw_flag == 1) {
			//g_send_data_info.ver_sw_flag = 0;
			g_send_data_info.ver_Compatibility_flag = 1;
			sprintf(g_send_data_info.cmd, "<ZKHYSET*PACKAGESIZE,%s,%d>\r\n", g_send_data_info.version, (int)(g_send_data_info.len));
			g_send_data_info.mode = SEND_CMD;

			/* 重新计算时间 */
			int useTime_t = (int)((g_send_data_info.len) / 4096.0);
				// baud:115200,use time
			g_send_data_info.time = (int)(useTime_t + useTime_t * 0.38 + 2);	// 旧版程序耗时

			char m_use_ime[DATA_LEN_50] = "";
			sprintf(m_use_ime, "预计耗时%d秒", g_send_data_info.time);
			ToShowInfo("");
			ToShowInfo(m_use_ime);
		}
		else {
			//g_send_data_info.ver_sw_flag = 1;
			g_send_data_info.ver_Compatibility_flag = 0;
			sprintf(g_send_data_info.cmd, "<ZKHYSET*PKGINFO,%s,%d,%d>\r\n", g_send_data_info.version, (int)(g_send_data_info.len), g_send_data_info.bin_type);
			g_send_data_info.mode = SEND_CMD;
		}
	}break;
	case 2: {	// 发送bin包数据时，超时判断
		if (g_send_data_info.is_dwnloading == true) {
			ToShowInfo("下载超时，请重新下载");
			g_send_data_info.is_dwnloading = false;
			// 使能获取版本号和关闭串口按钮
			m_btn_serial_sw_handle.EnableWindow(TRUE);
			m_btn_get_version_handle.EnableWindow(TRUE);
			KillTimer(2);
		}
	}break;
	default:break;
	}

	CDialogEx::OnTimer(nIDEvent);
}

void UsartWrite::OnEnChangeMfceditbrowse2()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
}



UINT UsartWrite::Thread_Can_Recv(void* param)
{
#define RECV_LEN		50
	UsartWrite* pthread = (UsartWrite*)param;
	VCI_CAN_OBJ frameinfo[RECV_LEN];
	VCI_ERR_INFO errinfo;
	int len = 0;
	printf("CAN Recv Thread Start.\n");

	while (!pthread->m_thread_recv_is_stop)
	{
		Sleep(1);
		len = VCI_Receive(m_devtype, m_devind, m_CANInd, frameinfo, RECV_LEN, 200);
		if (len <= 0)
		{
			//注意：如果没有读到数据则必须调用此函数来读取出当前的错误码，
			//千万不能省略这一步（即使你可能不想知道错误码是什么）
			VCI_ReadErrInfo(m_devtype, m_devind, m_CANInd, &errinfo);
		}
		else
		{
			for (int i = 0; i < len; i++)
			{
				//if (frameinfo[i].ID >= 0x750 && frameinfo[i].ID <= 0x758) {
				//	printf("0x%03X\n", frameinfo[i].ID);
				//}

				//如果是数据帧，获取数据帧里面的数据
				if (frameinfo[i].RemoteFlag == 0 && frameinfo[i].ExternFlag == 0)
				{
					//if (frameinfo[i].ID >= 0x750 && frameinfo[i].ID <= 0x758) {
					if (frameinfo[i].ID != 0) {
						printf("0x%03X\n", frameinfo[i].ID);
						pthread->m_canInfo_q.push(frameinfo[i]);
					}
				}
			}
			printf("len:%d\n", len);
			len = 0;
		}
	}
	printf("CAN Recv Thread End.\n");
	return 0;
}


UINT UsartWrite::Thread_Can_Process(void* param)
{
	/** 得到本类的指针 */
	UsartWrite* pthread = reinterpret_cast<UsartWrite*>(param);

	printf("CAN处理逻辑，开始\n");

	while (!pthread->m_thread_process_is_stop) {
		if (!pthread->m_canInfo_q.empty()) {
			VCI_CAN_OBJ l_canInfo = pthread->m_canInfo_q.front();
					
			switch(l_canInfo.ID){
			case SCREEN_REPLY_FRAME_ID: {	// 回复
				pthread->m_screen_upgrade_info.pkg_num = l_canInfo.Data[0];
				pthread->m_screen_upgrade_info.which_operation = l_canInfo.Data[1];
				pthread->m_screen_upgrade_info.screen_status = l_canInfo.Data[2];
				pthread->KillTimer(0);

				if (pthread->m_screen_upgrade_info.which_operation == 1) {		// 开始帧
					//printf("回复开始帧：");
					switch (pthread->m_screen_upgrade_info.screen_status) {
					case 0: {	// 正常
						// 发送数据
						pthread->g_send_data_info.send_cnt = pthread->m_screen_upgrade_info.pkg_num;
						pthread->g_send_data_info.mode = SEND_CAN_DATA_FRM;
					}break;
					case 2: {	// 重发
						printf("回复开始帧：异常-重发");
						// 若开始帧中收到重发，需要重发上一帧的数据
						pthread->g_send_data_info.send_cnt = pthread->m_screen_upgrade_info.pkg_num - 1;
						pthread->g_send_data_info.mode = SEND_CAN_START_FRM;
					}break;
					default: {	// 等待
						//printf("回复开始帧：等待");
					}break;
					}
				}
				else {	// 结束帧
					//printf("回复结束帧：");
					switch (pthread->m_screen_upgrade_info.screen_status) {
					case 0: {	// 正常
						// 发送下一包，或停止发送
						if (pthread->m_screen_upgrade_info.pkg_num == (pthread->g_send_data_info.times + 1) && \
							pthread->g_send_data_info.remain != 0) {
							// 关闭CAN
							if (g_can_is_open) {
								VCI_CloseDevice(m_devtype, m_devind);
								g_can_is_open = false;
								pthread->ToShowInfo("CAN已关闭");
								pthread->m_thread_send_pkg_is_stop = true;
								pthread->m_thread_recv_is_stop = true;
								pthread->m_thread_process_is_stop = true;
								pthread->ToShowInfo("小屏幕升级成功，可选择断电重启。");

								// 使能获取版本号和关闭串口按钮
								pthread->m_btn_serial_sw_handle.EnableWindow(TRUE);
								pthread->m_btn_get_version_handle.EnableWindow(TRUE);
								// 下载完成
								pthread->g_send_data_info.is_dwnloading = false;
								// 进度条设置成100%
								pthread->prog->SetPos(100);
							}
						}
						else if (pthread->m_screen_upgrade_info.pkg_num == pthread->g_send_data_info.times && \
							pthread->g_send_data_info.remain == 0) {
							// 关闭CAN
							if (g_can_is_open) {
								VCI_CloseDevice(m_devtype, m_devind);
								g_can_is_open = false;
								pthread->ToShowInfo("CAN已关闭");
								pthread->m_thread_send_pkg_is_stop = true;
								pthread->m_thread_recv_is_stop = true;
								pthread->m_thread_process_is_stop = true;
								pthread->ToShowInfo("小屏幕升级成功，可选择断电重启。");

								// 使能获取版本号和关闭串口按钮
								pthread->m_btn_serial_sw_handle.EnableWindow(TRUE);
								pthread->m_btn_get_version_handle.EnableWindow(TRUE);
								// 下载完成
								pthread->g_send_data_info.is_dwnloading = false;
								// 进度条设置成100%
								pthread->prog->SetPos(100);
							}
						}
						else {
							pthread->g_send_data_info.mode = SEND_CAN_START_FRM;
							// 发送下一包数据
							pthread->g_send_data_info.send_cnt = pthread->m_screen_upgrade_info.pkg_num + 1;
						}
					}break;
					case 2: {	// 重发
						printf("回复开始帧：异常-重发");
						pthread->g_send_data_info.send_cnt = pthread->m_screen_upgrade_info.pkg_num;
						pthread->g_send_data_info.mode = SEND_CAN_START_FRM;
					}break;
					default: {	// 等待
						//printf("回复开始帧：等待");
					}break;
					}
				}

			}break;
			default:break;
			}
			pthread->m_canInfo_q.pop();
		}
		else {
			Sleep(20);
		}
	}
	printf("CAN处理逻辑，结束\n");

	return 0;
}

UINT UsartWrite::Thread_Can_Upgrade_Logic(void* pParam)
{
#define CAN_SUB_DWN_TIME		1		// 单位ms

	printf("CAN 发送 线程开始\n");

	/** 得到本类的指针 */
	UsartWrite* pthread = reinterpret_cast<UsartWrite*>(pParam);

	// 获取升级包内容

	// 读取文件大小
	//CString fileName("");
	//CString fileName_t("");
	//pthread->m_EditBrowse.GetWindowTextA(pthread->m_fileName);
	//if (pthread->m_fileName.GetLength() < 5) {
	//	pthread->ToShowInfo("请先加载升级包~");
	//	return 1;
	//}

	if (g_can_is_open == false) {
		pthread->ToShowInfo("请先打开CAN~");
		return 1;
	}

	// 获取文件内容
	FILE* pFile;
	_wfopen_s(&pFile, pthread->m_fileName.AllocSysString(), L"rb"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
	if (pFile == NULL) //判断文件是否打开成功
	{
		pthread->MessageBox("fail to open", "warning", MB_ICONSTOP);
		return 1;
	}

	pthread->g_send_data_info.len = 0;
	fseek(pFile, 0, SEEK_END);//将文件指针设置到文件末尾处
	pthread->g_send_data_info.len = ftell(pFile);//获取文件指针的位置 也就相当于文件的大小了
	fseek(pFile, 0, SEEK_SET);//重新将文件指针调回文件开头

	// 释放掉动态分配的内存空间
	if (pthread->g_send_data_info.data != NULL) {
		free(pthread->g_send_data_info.data);
		pthread->g_send_data_info.data = NULL;
	}
	pthread->g_send_data_info.data = (unsigned char*)calloc(pthread->g_send_data_info.len, 1);
	if (pthread->g_send_data_info.data == NULL) {
		printf("data(len)申请内存失败\n");
		return 1;
	}
	fread(pthread->g_send_data_info.data, 1, pthread->g_send_data_info.len, pFile);//将整个文件读取 注意这里文件的大小不应超过65536
	fclose(pFile);//关闭文件

	// 计算文件匹配，否则提示 bin_type
	if (pthread->g_send_data_info.len > 1024 * 64) {	// 默认小屏幕升级包最大为32KB
		pthread->ToShowInfo("请加载Screen的Bin升级包。");
		return 1;
	}
	printf("pkg len:%d\n", pthread->g_send_data_info.len);
	
	unsigned char *data = NULL;
	// 是否是有效升级包
	if (pthread->g_send_data_info.data[0] == 0x00 && pthread->g_send_data_info.data[1] == 0x11 && pthread->g_send_data_info.data[2] == 0x22 && \
		pthread->g_send_data_info.data[pthread->g_send_data_info.len - 3] == 0xDD && pthread->g_send_data_info.data[pthread->g_send_data_info.len - 2] == 0xEE \
		&& pthread->g_send_data_info.data[pthread->g_send_data_info.len - 1] == 0xFF)
	{
		pthread->g_send_data_info.mode = SEND_CAN_START_FRM;
		// 取出有效的Bin包
		data = pthread->g_send_data_info.data + 8;
		pthread->g_send_data_info.len -= 20;
		pthread->g_send_data_info.times = pthread->g_send_data_info.len / PKG_SCREEN_SIZE;
		pthread->g_send_data_info.remain = pthread->g_send_data_info.len % PKG_SCREEN_SIZE;
		pthread->g_send_data_info.send_cnt = 1;
		pthread->g_send_data_info.checkout = 0;

		if (pthread->g_send_data_info.remain > 0)
			pthread->g_send_data_info.times += 1;

		// 计算耗时
		pthread->g_send_data_info.time = pthread->g_send_data_info.times * 0.75;
		char m_use_ime[DATA_LEN_50] = "";
		sprintf(m_use_ime, "预计耗时%d秒", pthread->g_send_data_info.time);
		pthread->ToShowInfo("");
		pthread->ToShowInfo(m_use_ime);

		printf("times:%d,remain:%d\n", pthread->g_send_data_info.times, pthread->g_send_data_info.remain);

		pthread->g_send_data_info.is_dwnloading = true;	// 升级中

		uint8_t loop_times = 0;
		while (!pthread->m_thread_send_pkg_is_stop) {

			switch (pthread->g_send_data_info.mode) {
			case No_Status: {		// 等待				
				Sleep(20);
				if (loop_times >= 250) {	// 等待5秒后
					pthread->ToShowInfo("下载超时，请重新下载");
					goto TO_CAN_SEND_END;
				}
				else loop_times++;
			}break;
			case SEND_CAN_START_FRM: {		// CAN 发送起始帧
				pthread->g_send_data_info.mode = No_Status;

				// 发送CAN起始数据包
				memset(&pthread->m_can_send_pbj, 0, sizeof(VCI_CAN_OBJ));

				pthread->m_can_send_pbj.ID = SCREEN_SUB_START_FRAME_ID;
				pthread->m_can_send_pbj.DataLen = 8;

				// 包ID
				pthread->m_can_send_pbj.Data[0] = pthread->g_send_data_info.send_cnt & 0xFF;
				// 一共分几次下发
				if (pthread->g_send_data_info.remain > 0)
					pthread->m_can_send_pbj.Data[1] = pthread->g_send_data_info.times + 1;
				else
					pthread->m_can_send_pbj.Data[1] = pthread->g_send_data_info.times;

				// 单包大小
				pthread->m_can_send_pbj.Data[2] = PKG_SCREEN_SIZE & 0xFF;
				pthread->m_can_send_pbj.Data[3] = PKG_SCREEN_SIZE >> 8;
				// 包总长度
				pthread->m_can_send_pbj.Data[4] = (BYTE)(pthread->g_send_data_info.len & 0xFF);
				pthread->m_can_send_pbj.Data[5] = (BYTE)((pthread->g_send_data_info.len >> 8) & 0xFF);
				pthread->m_can_send_pbj.Data[6] = (BYTE)((pthread->g_send_data_info.len >> 16) & 0xFF);
				pthread->m_can_send_pbj.Data[7] = (BYTE)((pthread->g_send_data_info.len >> 24) & 0xFF);\

				bool ret = pthread->CANSendData(pthread->m_can_send_pbj.ID, pthread->m_can_send_pbj.DataLen, pthread->m_can_send_pbj.Data, \
					DataFrame, StandardFrame, NormalSend);
				if (!ret) {
					pthread->m_thread_send_pkg_is_stop = true;
					pthread->m_thread_recv_is_stop = true;
					pthread->m_thread_process_is_stop = true;
					if (g_can_is_open) VCI_CloseDevice(m_devtype, m_devind);
					g_can_is_open = false;
					pthread->ToShowInfo("CAN数据(起始帧)发送失败，CAN已关闭，就重新下载");

					// 进度条设置成100%
					pthread->prog->SetPos(0);
					break;
				}
				loop_times = 0;
				printf("发送起始帧数据.\n");
				// 超时操作
				pthread->SetTimer(0, 1000, NULL);
			}break;
			case SEND_CAN_DATA_FRM: {			// CAN 发送数据
				// 每次发送时的buf数据的下标
				int data_index = (pthread->g_send_data_info.send_cnt - 1) * PKG_SCREEN_SIZE;

				// 分包验证 并 下发
				memset(&pthread->m_can_send_pbj, 0, sizeof(VCI_CAN_OBJ));

				pthread->m_can_send_pbj.ID = SCREEN_SUB_DWN_FRAME_ID;
				pthread->m_can_send_pbj.DataLen = 8;

				pthread->g_send_data_info.checkout = 0;
				for (int i = 0; i < PKG_SCREEN_SIZE / 8; i++) {
					memset(pthread->m_can_send_pbj.Data, 0, 8);
					// 数据
					for (UINT8 j = 0; j < 8; j++) {
						pthread->m_can_send_pbj.Data[j] = data[data_index++];
						pthread->g_send_data_info.checkout += pthread->m_can_send_pbj.Data[j];				// 单包校验值
					}
					bool ret = pthread->CANSendData(pthread->m_can_send_pbj.ID, pthread->m_can_send_pbj.DataLen, pthread->m_can_send_pbj.Data, \
						DataFrame, StandardFrame, NormalSend);
					if (!ret) {
						if (g_can_is_open) VCI_CloseDevice(m_devtype, m_devind);
						pthread->m_thread_send_pkg_is_stop = true;
						pthread->m_thread_recv_is_stop = true;
						pthread->m_thread_process_is_stop = true;
						g_can_is_open = false;
						pthread->ToShowInfo("CAN数据(数据帧)发送失败，CAN已关闭,就重新下载");

						// 进度条设置成100%
						pthread->prog->SetPos(0);
						break;
					}
					loop_times = 0;
					Sleep(CAN_SUB_DWN_TIME);		// 分包下发时，包与包之间的时间间隔
				}
				// 发送结束包
				pthread->g_send_data_info.mode = SEND_CAN_END_FRM;
			}break;
			case SEND_CAN_END_FRM: {			// CAN发送结束帧
				pthread->g_send_data_info.mode = No_Status;

				// 发送CAN起始数据包
				memset(&pthread->m_can_send_pbj, 0, sizeof(VCI_CAN_OBJ));

				pthread->m_can_send_pbj.ID = SCREEN_SUB_END_FRAME_ID;
				pthread->m_can_send_pbj.DataLen = 8;

				// 包ID
				pthread->m_can_send_pbj.Data[0] = pthread->g_send_data_info.send_cnt & 0xFF;
				// 一共分几次下发
				pthread->m_can_send_pbj.Data[1] = pthread->g_send_data_info.times;

				// 包校验值
				pthread->m_can_send_pbj.Data[2] = pthread->g_send_data_info.checkout & 0xFF;
				pthread->m_can_send_pbj.Data[3] = (pthread->g_send_data_info.checkout >> 8) & 0xFF;
				pthread->m_can_send_pbj.Data[4] = (pthread->g_send_data_info.checkout >> 16) & 0xFF;
				pthread->m_can_send_pbj.Data[5] = (pthread->g_send_data_info.checkout >> 24) & 0xFF;

				
				bool ret = pthread->CANSendData(pthread->m_can_send_pbj.ID, pthread->m_can_send_pbj.DataLen, pthread->m_can_send_pbj.Data, \
					DataFrame, StandardFrame, NormalSend);
				if (!ret) {
					if (g_can_is_open) VCI_CloseDevice(m_devtype, m_devind);
					pthread->m_thread_send_pkg_is_stop = true;
					pthread->m_thread_recv_is_stop = true;
					pthread->m_thread_process_is_stop = true;
					g_can_is_open = false;
					pthread->ToShowInfo("CAN数据(结束帧)发送失败，CAN已关闭,就重新下载");

					// 进度条设置成100%
					pthread->prog->SetPos(0);
					break;
				}
				// 进度条显示
				int pos = (int)((pthread->g_send_data_info.send_cnt) * 1.0 / pthread->g_send_data_info.times * 100);
				pthread->prog->SetPos(pos);

				// 超时操作
				loop_times = 0;
				pthread->SetTimer(0, 1000, NULL);
			}break;
			}
		}
	}
	else {
		printf("解析升级包失败.\n");
	}

TO_CAN_SEND_END:
	pthread->g_send_data_info.is_dwnloading = false;
	pthread->m_thread_send_pkg_is_stop = true;
	// 使能获取版本号和关闭串口按钮
	pthread->m_btn_serial_sw_handle.EnableWindow(TRUE);
	pthread->m_btn_get_version_handle.EnableWindow(TRUE);

	printf("CAN 发送 线程结束\n");
	return 0;
}

/*****************************************
函数说明：CAN发送数据
参数1：帧ID
参数2：发送数据长度
参数3：发送数据
参数4：帧格式
参数5：帧类型
参数6：发送类型
函数返回值：
*****************************************/
bool UsartWrite::CANSendData(UINT FrameID, BYTE DataLen, BYTE* Data, FrameFormat FrameFmt, FrameType FrameTp, SendType SendTp)
{
	VCI_CAN_OBJ frameinfo;

	frameinfo.DataLen = DataLen;			// 发送数据长度
	frameinfo.RemoteFlag = FrameFmt;			// 发送帧格式：0：数据帧 | 1：远程帧
	frameinfo.ExternFlag = FrameTp;			// 发送帧类型：0：标准帧 | 1：扩展帧
	frameinfo.ID = FrameID;			// 帧ID
	frameinfo.SendType = SendTp;			// 发送类型：0：正常发送 | 1：单次发送 | 2：自收自发 | 3：单次自收自发
	memcpy(&frameinfo.Data, Data, DataLen);		// 发送数据
	printf("%03X\n", FrameID);
	if (VCI_Transmit(m_devtype, m_devind, m_CANInd, &frameinfo, 1) != STATUS_OK)
	{
		cout << "写入失败" << endl;
		return false;
	}
	//else
	//{
	//	cout << "写入成功" << endl;
	//}
	return true;
}