#pragma once
/*****************结构体*************************/
typedef struct {
    int speed;
    int aebStatus;
    bool aebSw;
    bool rLed;
    bool lLed;
    bool brakeLed;
    int  gear;      // 档位信息
    int errId;      // 故障码

    bool show;      // 显示标志位
}VBase_Info;

typedef struct {
    int speed;          // 速度
    float ttcTime;      // TTC
    float hmwTime;      // HMW
    float LongRelDis;   // 纵向相对距离
    float LongRelVel;   // 纵向相对速度
    float TranRelDis;   // 横向相对距离
    float ThrottleOpening;  // 油门踏板开度
    float BrakeOpening;     // 刹车踏板开度
    int SWADegree;          // 方向盘角度
    bool Direction;         // 方向盘状态
    int LeftLaneStyle;      // 左车道线类型
    int RightLaneStyle;     // 右车道线类型
    int FCWLevel;           // 前向碰撞预警等级
    int ObstacleType;       // 障碍物类型
    int RightLDW;           // 右车道线偏离预警
    int LeftLDW;            // 左车道线偏离预警
    int BreakFlag;          //  AEB制动状态【0：无制动】、【1：双目制动】、【2：毫米波制动】、【3：超声波制动】
    int Car_Gear;           // 档位信息【0：无效】、【1：N档】、【2：D档】、【3：R档】
    int eventType;          // 事件类型：【1：FCW】、【2：HMW】、【3：LDW】、【4：AEB】
    bool eventFlag;         // 对应事件开始与结束
    int Ultrasonicdistance; // 超声波距离 分米

    bool show;      // 显示标志位
}Warning_Info;

typedef struct {
    int speed;
    bool aebSw;
    bool rLed;
    bool lLed;
    bool brakeLed;
    int  gear;      // 档位信息
    int errId;      // 故障码
    float ThrottleOpening;  // 油门踏板开度
    float BrakeOpening;     // 刹车踏板开度
    int SWADegree;          // 方向盘角度
    bool Direction;         // 方向盘状态

    bool show;      // 显示标志位
}VBody_Info;

typedef struct {
    int speed;      // GPS车速  KM/H
    bool isValid;   // 车速信息是否有效 【0：无效】、【1：有效】

    bool show;      // 显示标志位
}Gps_Info;

#define VBASE_LEN       9
#define WARNING_LEN     24 
#define VBODY_LEN       13
#define GPS_LEN         6

typedef struct {
    unsigned char data[VBASE_LEN];
}VBase_Data;
typedef struct {
    unsigned char data[WARNING_LEN];
}Warning_Data;
typedef struct {
    unsigned char data[VBODY_LEN];
}VBody_Data;
typedef struct {
    unsigned char data[GPS_LEN];
}GPS_Data;