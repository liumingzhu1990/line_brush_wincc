﻿// Encode.cpp: 实现文件
//

#include "pch.h"
#include "USART_Upgrade.h"
#include "Encode.h"
#include "afxdialogex.h"
#include "configure.h"
//#define BACKGOUND2			RGB(170, 198, 91)
#define BACKGOUND2			RGB(150, 201, 137)
// Encode 对话框

IMPLEMENT_DYNAMIC(Encode, CDialogEx)

Encode::Encode(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
{

}

Encode::~Encode()
{
}

void Encode::DoDataExchange(CDataExchange* pDX)
{
	DDX_Control(pDX, IDC_MFCEDITBROWSE1, m_EditBrowse1);
	DDX_Control(pDX, IDC_SPIN_1, m_Spin1);
	DDX_Control(pDX, IDC_SPIN_2, m_Spin2);
	DDX_Control(pDX, IDC_SPIN_3, m_Spin3);
	DDX_Control(pDX, IDC_SPIN_4, m_Spin4);
	DDX_Control(pDX, IDC_SPIN_5, m_Spin5);
	DDX_Control(pDX, IDC_SPIN_11, m_Spin11);
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Encode, CDialogEx)
	ON_BN_CLICKED(IDC_Encode_btn, &Encode::OnBnClickedEncodebtn)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_1, &Encode::OnDeltaposSpin1)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_2, &Encode::OnDeltaposSpin2)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_3, &Encode::OnDeltaposSpin3)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_4, &Encode::OnDeltaposSpin4)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_5, &Encode::OnDeltaposSpin5)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_11, &Encode::OnDeltaposSpin11)
END_MESSAGE_MAP()


// Encode 消息处理程序

//unsigned char data[1024 * 100] = "";
void Encode::OnBnClickedEncodebtn()
{
	// 获取文件路径
	CString fileName("");
	CString fileName_t("");
	int len = 0;
	
	unsigned char *data_new = NULL;
	unsigned char *data = NULL;
	int sum = 0;
	unsigned char checkout[4] = { 0 };
	
	m_EditBrowse1.GetWindowTextA(fileName);
	if (fileName.GetLength() < 5) {
		((CComboBox*)GetDlgItem(IDC_STAT_INFO))->SetWindowTextA("请先加载升级包~");
		return;
	}
	// 获取文件路径
	fileName_t = fileName;
	CString path("");
	char* p = strtok(fileName_t.GetBuffer(), "\\");
	int pos = 0, pos_t = 0;
	while (p) {
		pos++;
		p = strtok(NULL, "\\");
	}
	fileName_t = fileName;
	p = strtok(fileName_t.GetBuffer(), "\\");
	while (p) {
		if(pos_t < (pos-1))
			path = path + p + "\\";
		p = strtok(NULL, "\\");
		pos_t++;
	}
	//printf("last:%s,%d\n", fileName.Right(fileName.GetLength() - fileName.ReverseFind('.') - 1), fileName.ReverseFind('.'));
	CString subfix = fileName.Right(fileName.GetLength() - fileName.ReverseFind('.') - 1);	// 获取后缀
	if (!subfix.Compare("hex")) {
		printf("hex\n");
		// hex文件的加密方式：1将hex转成bin文件；2将bin文件直接加密
		data = (unsigned char*)malloc(500 * 1024);
		len = Encode_Hex2Bin(fileName,data)+1;
#if 0
		int cnt = 0;
		int times = 71*1024/ 16;
		for (int i = 0; i < times; i++) {
			printf("%02X0\t", cnt);
			for (int j = 0; j < 16; j++) {
				printf("%02X ", data[i * 16 + j]);
			}
			printf("\n");
			cnt++;
		}
		
		printf("%d,%dKB\n", strlen((char*)data), strlen((char*)data) / 1024);
#endif
	}
	else
	{
		printf("bin\n");
		FILE* pFile;
		_wfopen_s(&pFile, fileName.AllocSysString(), L"rb"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
		if (pFile == NULL) //判断文件是否打开成功
		{
			MessageBox("fail to open", "warning", MB_ICONSTOP);
			return;
		}
		fseek(pFile, 0, SEEK_END);		// 将文件指针设置到文件末尾处
		len = ftell(pFile)+1;				// 获取文件指针的位置 也就相当于文件的大小了
		fseek(pFile, 0, SEEK_SET);		// 重新将文件指针调回文件开头
		data = (unsigned char*)malloc(len * sizeof(signed char));
		memset(data, 0, len * sizeof(unsigned char));
		fread(data, sizeof(unsigned char), len, pFile);//将整个文件读取 注意这里文件的大小不应超过65536
		fclose(pFile);//关闭文件
		// bin文件的加密方式
	}

	// 获取文件名称 V1.1.0.L1C6
	char version[20] = { 0 };
	CString Version(""), V(""), LS(""), C(""), H(""), M_v(""), S_v(""), R_v(""), LS_n(""), C_n(""), H_n("");
	((CComboBox*)GetDlgItem(IDC_COMBO_1))->GetWindowTextA(V);
	((CComboBox*)GetDlgItem(IDC_COMBO_2))->GetWindowTextA(LS);
	((CComboBox*)GetDlgItem(IDC_COMBO_3))->GetWindowTextA(C);
	((CComboBox*)GetDlgItem(IDC_COMBO_4))->GetWindowTextA(H);
	((CEdit*)GetDlgItem(IDC_EDIT_1))->GetWindowTextA(M_v);
	((CEdit*)GetDlgItem(IDC_EDIT_2))->GetWindowTextA(S_v);
	((CEdit*)GetDlgItem(IDC_EDIT_3))->GetWindowTextA(R_v);
	((CEdit*)GetDlgItem(IDC_EDIT_4))->GetWindowTextA(LS_n);
	((CEdit*)GetDlgItem(IDC_EDIT_5))->GetWindowTextA(C_n);
	((CEdit*)GetDlgItem(IDC_EDIT_11))->GetWindowTextA(H_n);
	Version = V + M_v+"."+S_v + "." + R_v + "." + LS + LS_n + C + C_n + H + H_n;
	//printf("%s\n", Version.GetBuffer());
#ifdef ENCODE_OPERARION
	// 判断是否已经是加密包
	if (data[0] == 0x00 && data[1] == 0x11 && data[2] == 0x22 && \
		data[len - 3] == 0xDD && data[len - 2] == 0xEE && data[len - 1] == 0xFF)
	{
		((CComboBox*)GetDlgItem(IDC_STAT_INFO))->SetWindowTextA("已是升级包，无需加密~");
		free(data);
		data = NULL;
		return;
	}
	// 文件加密
	for (int i = 0; i < len; i++) {
		sum = (sum + data[i])&0xFFFFFFFF;
	}
	checkout[3] = sum & 0xFF;
	checkout[2] = (sum>>8) & 0xFF;
	checkout[1] = (sum>>16) & 0xFF;
	checkout[0] = (sum>>24) & 0xFF;
	//printf("%02X %02X %02X %02X\n", checkout[3],checkout[2],checkout[1],checkout[0]);

	data_new = (unsigned char*)malloc((len + 20) * sizeof(signed char));
	memset(data_new, 0, (len + 20));
	unsigned char front[8] =	{0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77};
	unsigned char end[8] =		{0x88,0x99,0xAA,0xBB,0xCC,0xDD,0xEE,0xFF};
	memcpy(data_new, front, 8);
	memcpy(data_new+8, data, len);
	memcpy(data_new+len+8, checkout, 4);
	memcpy(data_new+len+8+4, end, 8);
	//for (int i = 0; i < (len+20); i++) {
	//	printf("%02X ", data_new[i]);
	//}
	//printf("\n");
#else
	data_new = (unsigned char*)malloc(len * sizeof(signed char));
	memset(data_new, 0, len);
	memcpy(data_new, data, len);
#endif
	// 写文件
	fileName = path + Version + ".bin";
	//printf("%s\n",fileName.GetBuffer());
	FILE* pFile_w;
	_wfopen_s(&pFile_w, fileName.AllocSysString(), L"wb"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
	if (pFile_w == NULL) //判断文件是否打开成功
	{
		MessageBox("fail to open", "warning", MB_ICONSTOP);
		return;
	}
#ifdef ENCODE_OPERARION
	fwrite(data_new, sizeof(unsigned char), (len+20), pFile_w);//将整个文件读取 注意这里文件的大小不应超过65536
#else
	fwrite(data_new, sizeof(unsigned char), (len), pFile_w);//将整个文件读取 注意这里文件的大小不应超过65536
#endif
	fclose(pFile_w);//关闭文件
	// 显示生成文件的路径
	((CEdit*)GetDlgItem(IDC_EDIT1))->SetWindowTextA(fileName);
	((CComboBox*)GetDlgItem(IDC_STAT_INFO))->SetWindowTextA("升级包加密完成");
	free(data);
	data = NULL;
	free(data_new);
	data_new = NULL;


	// 生成一个文件upgradeInfo.txt文件。记录 文件名|文件字节大小|文件版本号
	FILE* pFile_Info;
	char* data_info = (char *)malloc(sizeof(char) * 40);
	
	fileName = path + "upgradeInfo.txt";
	_wfopen_s(&pFile_Info, fileName.AllocSysString(), L"w"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
	if (pFile_Info == NULL) //判断文件是否打开成功
	{
		MessageBox("fail to open", "warning", MB_ICONSTOP);
		return;
	}
	CString ver_w = Version + ".bin";
#ifdef ENCODE_OPERARION
	sprintf(data_info,"%d\n\n%s\n",len+20, T2A(ver_w.GetBuffer(0)));
#else
	sprintf(data_info, "%d\n\n%s\n", len, T2A(ver_w.GetBuffer(0)));
#endif
	fwrite(data_info, sizeof(char), strlen(data_info), pFile_Info);//将整个文件读取 注意这里文件的大小不应超过65536
	fclose(pFile_Info);//关闭文件
	free(data_info);
}


BOOL Encode::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	SetBackgroundColor(BACKGOUND2);//设置显示框的颜色
	m_EditBrowse1.EnableFileBrowseButton(_T(""), _T("Hex Files(*.hex)|*.hex|Bin Files(*.bin)|*.bin|All Files (*.*)|*.*||"));

	initControlFontSize();
	initDlgContent();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}

void Encode::initControlFontSize()
{
	mySetFontSize(IDC_Encode_btn, 1.8);		// 加密
	mySetFontSize(IDC_STA_VER, 1.8);		// 设置版本文字
	mySetFontSize(IDC_STAT_INFO, 1.8);		// 文字提示
	mySetFontSize(IDC_EDIT1, 1.3);			// 加密完毕路径显示
	mySetFontSize(IDC_MFCEDITBROWSE1, 1.3);	// 浏览文件
	mySetFontSize(IDC_COMBO_1, 1.8);		// 下拉 V
	mySetFontSize(IDC_COMBO_2, 1.8);		// 下拉 L S
	mySetFontSize(IDC_COMBO_3, 1.8);		// 下拉 C
	mySetFontSize(IDC_COMBO_4, 1.8);		// 下拉 H

	mySetFontSize(IDC_EDIT_1, 1.8);			// 主版本
	mySetFontSize(IDC_EDIT_2, 1.8);			// 次版本
	mySetFontSize(IDC_EDIT_3, 1.8);			// 修订版本
	mySetFontSize(IDC_EDIT_4, 1.8);			// 几个相机
	mySetFontSize(IDC_EDIT_5, 1.8);			// 几个超声波雷达
	mySetFontSize(IDC_EDIT_11, 1.8);		// 几个毫米波雷达
}
void Encode::initDlgContent()
{
	((CComboBox*)GetDlgItem(IDC_COMBO_1))->AddString("V");
	((CComboBox*)GetDlgItem(IDC_COMBO_1))->SetCurSel(0);

	((CComboBox*)GetDlgItem(IDC_COMBO_2))->AddString("L");
	((CComboBox*)GetDlgItem(IDC_COMBO_2))->AddString("S");
	((CComboBox*)GetDlgItem(IDC_COMBO_2))->SetCurSel(0);

	((CComboBox*)GetDlgItem(IDC_COMBO_3))->AddString("C");
	((CComboBox*)GetDlgItem(IDC_COMBO_3))->SetCurSel(0);

	((CComboBox*)GetDlgItem(IDC_COMBO_4))->AddString("H");
	((CComboBox*)GetDlgItem(IDC_COMBO_4))->SetCurSel(0);
	cnt1 = cnt2 = cnt4 = cnt5 = 1;
	cnt3 = 0;
	cnt11 = 0;
	((CComboBox*)GetDlgItem(IDC_EDIT_1))->SetWindowTextA("1");
	((CComboBox*)GetDlgItem(IDC_EDIT_2))->SetWindowTextA("0");
	((CComboBox*)GetDlgItem(IDC_EDIT_3))->SetWindowTextA("0");
	((CComboBox*)GetDlgItem(IDC_EDIT_4))->SetWindowTextA("1");
	((CComboBox*)GetDlgItem(IDC_EDIT_5))->SetWindowTextA("1");
	((CComboBox*)GetDlgItem(IDC_EDIT_11))->SetWindowTextA("0");
}
bool Encode::mySetFontSize(int ID, float times)
{
	LOGFONT   logfont;//最好弄成类成员,全局变量,静态成员  
	CFont* pfont1 = GetDlgItem(ID)->GetFont();
	pfont1->GetLogFont(&logfont);
	logfont.lfHeight = logfont.lfHeight * times;   //这里可以修改字体的高比例
	logfont.lfWidth = logfont.lfWidth * times;   //这里可以修改字体的宽比例
	static   CFont   font1;
	font1.CreateFontIndirect(&logfont);
	GetDlgItem(ID)->SetFont(&font1);
	font1.Detach();

	return TRUE;
}


void Encode::OnDeltaposSpin1(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt1++;
		if (cnt1 >= 100) cnt1 = 100;
	}
	else {							// -
		cnt1--;
		if (cnt1 < 0) cnt1 = 0;
	}
	m_editNum.Format("%d", cnt1);
	((CComboBox*)GetDlgItem(IDC_EDIT_1))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void Encode::OnDeltaposSpin2(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt2++;
		if (cnt2 >= 100) cnt2 = 100;	
	}
	else {							// -
		cnt2--;
		if (cnt2 < 0) cnt2 = 0;
	}
	m_editNum.Format("%d", cnt2);
	((CComboBox*)GetDlgItem(IDC_EDIT_2))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void Encode::OnDeltaposSpin3(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt3++;
		if (cnt3 >= 100) cnt3 = 100;
	}
	else {							// -
		cnt3--;
		if (cnt3 < 0) cnt3 = 0;
	}
	m_editNum.Format("%d", cnt3);
	((CComboBox*)GetDlgItem(IDC_EDIT_3))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void Encode::OnDeltaposSpin4(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt4++;
		if (cnt4 >= 100) cnt4 = 100;
	}
	else {							// -
		cnt4--;
		if (cnt4 < 0) cnt4 = 0;
	}
	m_editNum.Format("%d", cnt4);
	((CComboBox*)GetDlgItem(IDC_EDIT_4))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void Encode::OnDeltaposSpin5(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt5++;
		if (cnt5 >= 100) cnt5 = 100;
	}
	else {							// -
		cnt5--;
		if (cnt5 < 0) cnt5 = 0;
	}
	m_editNum.Format("%d", cnt5);
	((CComboBox*)GetDlgItem(IDC_EDIT_5))->SetWindowTextA(m_editNum);
	*pResult = 0;
}


void Encode::OnDeltaposSpin11(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		cnt11++;
		if (cnt11 >= 100) cnt11 = 100;
	}
	else {							// -
		cnt11--;
		if (cnt11 < 0) cnt11 = 0;
	}
	m_editNum.Format("%d", cnt11);
	((CComboBox*)GetDlgItem(IDC_EDIT_11))->SetWindowTextA(m_editNum);
	*pResult = 0;
}

unsigned char ChartoByte(char c)
{
	if (c - 'a' >= 0) return(c - 'a' + 10);
	else if (c - 'A' >= 0) return(c - 'A' + 10);
	else return(c - '0');
}

unsigned char Char2toByte(char* s)
{
	return (ChartoByte(*s) * 16 + ChartoByte(*(s + 1)));
}




int Encode::Encode_Hex2Bin(CString fileName,unsigned char *data)
{
	// 读取文件
	FILE* pFile;
	_wfopen_s(&pFile, fileName.AllocSysString(), L"rb"); //打开文件strFilePath是文件路径VS2010是UNICODE编码 定义时注意转换或者这样L"xxx"
	if (pFile == NULL) //判断文件是否打开成功
	{
		MessageBox("fail to open", "warning", MB_ICONSTOP);
		return false;
	}
	// 1将hex转成bin文件
	unsigned char length = 0;
	unsigned long offset = 0;  //0~65535
	unsigned char type = 0;
	char buff[64] = "";
	unsigned char tmp[16] = "";
	int cnt = 0;
	int ii = 0;
	while (1)
	{
		fgets(buff, 64, pFile);

		if (feof(pFile)) break; //文件结束
		else if (buff[0] != ':') continue; //无效行
		else if (strcmp(buff, ":00000001FF\n") == 0) {
			printf("end.\n"); break; //结束行
		}
		else
		{
			length = Char2toByte(&buff[1]);
			offset = Char2toByte(&buff[3]) * 256 + Char2toByte(&buff[5]);
			type = Char2toByte(&buff[7]);

			if (type == 0)
			{			
				for (int i = 0; i < length; i++) {	// 16
					tmp[i] = (Char2toByte(&buff[9 + 2 * i]))&0xFF;
				}
				if (length == 4) {
					if (tmp[0] == 0x5A && tmp[1] == 0x5A && tmp[2] == 0xA5 && tmp[3] == 0xA5) {
						continue;
					}
					if (tmp[0] == 0x21 && tmp[1] == 0x43 && tmp[2] == 0x65 && tmp[3] == 0x87) {
						continue;
					}
				}
				for (int i = 0; i < length;i++) {
					data[ii] = tmp[i];
					ii++;
				}
			}
		}
	}
	fclose(pFile);//关闭文件

	// 不满足1024整数倍的包需要在剩余包中添加0xFF
	int remain = ii % 1024;
	//printf("%d\n", remain);
	if (remain) {
		for (int i = 0; i < (1024 - remain); i++) {
			data[ii] = 0xFF;
			ii++;
		}
	}

	// 测试打印
#if 0
	int times = ii / 16;
	for (int i = 0; i < times; i++) {
		printf("%02X0\t", cnt);
		for (int j = 0; j < 16;j++) {
			printf("%02X ", data[i * 16 + j]);
		}
		printf("\n");
		cnt++;
	}
	printf("%d,%dKB\n",ii,ii/1024);
#endif
	// 2将bin文件直接加密
	return ii - 1;
}



