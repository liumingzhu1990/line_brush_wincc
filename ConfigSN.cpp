﻿// ConfigSN.cpp: 实现文件
//

#include "pch.h"
#include "USART_Upgrade.h"
#include "ConfigSN.h"
#include "afxdialogex.h"
#include <iostream>



using namespace std;
#define BACKGOUND3			RGB(150, 181, 137)
// ConfigSN 对话框
DWORD WINAPI Thread_Serial_r(LPVOID lpParam);

IMPLEMENT_DYNAMIC(ConfigSN, CDialogEx)

ConfigSN::ConfigSN(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG3, pParent)
{

}

ConfigSN::~ConfigSN()
{
	if (start_flag == true) {
		start_flag = false;
		mCommUtils.CloseCom();
		//TerminateThread(thread_r, 0);
		CloseHandle(thread_r);
	}
}

void ConfigSN::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(ConfigSN, CDialogEx)
	ON_BN_CLICKED(IDC_BUT_Wrt_SN, &ConfigSN::OnBnClickedButWrtSn)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_Year, &ConfigSN::OnDeltaposSpinYear)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_Month, &ConfigSN::OnDeltaposSpinMonth)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_Day, &ConfigSN::OnDeltaposSpinDay)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_SNID, &ConfigSN::OnDeltaposSpinSnid)
	ON_BN_CLICKED(IDC_BUT_OpenCom, &ConfigSN::OnBnClickedButOpencom)
	ON_BN_CLICKED(IDC_BUT_READ_SN, &ConfigSN::OnBnClickedButReadSn)
END_MESSAGE_MAP()


// ConfigSN 消息处理程序


BOOL ConfigSN::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	SetBackgroundColor(BACKGOUND3);//设置显示框的颜色
	InitControlFontSize();
	SeekSerial();
	FillInData();	// 填数

	GetDlgItem(IDC_BUT_Wrt_SN)->EnableWindow(FALSE);
	GetDlgItem(IDC_BUT_READ_SN)->EnableWindow(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}

void ConfigSN::InitControlFontSize()
{
	MySetFontSize(IDC_BUT_OpenCom, 1.8f);	// 打开串口
	MySetFontSize(IDC_BUT_Wrt_SN, 1.8f);		// 写
	MySetFontSize(IDC_BUT_READ_SN, 1.8f);	// 读
	MySetFontSize(IDC_STA_Dis_SN, 2.2f);		// 读取/显示SN号

	MySetFontSize(IDC_EDIT_Year, 1.8f);		// 年
	MySetFontSize(IDC_EDIT_Month, 1.8f);		// 月
	MySetFontSize(IDC_EDIT_Day, 1.8f);		// 日
	MySetFontSize(IDC_EDIT_SNID, 1.8f);		// SN ID

	MySetFontSize(IDC_COM, 1.8f);			// 串口标识
	MySetFontSize(IDC_STA_Warn, 2.2f);		// 提示信息


}

bool ConfigSN::MySetFontSize(int ID, float times)
{
	LOGFONT   logfont;//最好弄成类成员,全局变量,静态成员  
	CFont* pfont1 = GetDlgItem(ID)->GetFont();
	pfont1->GetLogFont(&logfont);
	logfont.lfHeight = (LONG)(logfont.lfHeight * times);   //这里可以修改字体的高比例
	logfont.lfWidth = (LONG)(logfont.lfWidth * times);   //这里可以修改字体的宽比例
	static   CFont   font1;
	font1.CreateFontIndirect(&logfont);
	GetDlgItem(ID)->SetFont(&font1);
	font1.Detach();

	return TRUE;
}

bool ConfigSN::FillInData()
{
	// 获取年月日 填数
	CTime time;
	time = CTime::GetCurrentTime();
	//CString strNowTime = time.Format("%Y-%m-%d %H:%M:%S");//获取当前时间
	year = time.GetYear() % 2000;
	month = time.GetMonth();
	day = time.GetDay();
	CString str;
	str.Format("%d", year);
	((CComboBox*)GetDlgItem(IDC_EDIT_Year))->SetWindowTextA(str);
	str.Format("%02d", month);
	((CComboBox*)GetDlgItem(IDC_EDIT_Month))->SetWindowTextA(str);
	str.Format("%02d", day);
	((CComboBox*)GetDlgItem(IDC_EDIT_Day))->SetWindowTextA(str);
	str.ReleaseBuffer();

	// 从sn.dat文件都读取记忆数据
	// 存储数据：21-12-27-00001
	char readBuf[20] = { 0 };
	FILE* fp = fopen("./sn.dat","ab+");
	fread(readBuf,1,sizeof(readBuf),fp);
	readBuf[strlen(readBuf)] = '\0';
	//cout << "read:" << readBuf << endl;	
	fclose(fp);
	int year_t = 0, month_t = 0, day_t = 0;
	if (strlen(readBuf) < 1) {
		str.Format("%05d", 1);
		((CComboBox*)GetDlgItem(IDC_EDIT_SNID))->SetWindowTextA(str);
		str.Format("今天还没有写过。");
		GetDlgItem(IDC_STA_Warn)->SetWindowTextA(str);
		return true;
	}
	sscanf(readBuf,"%d-%d-%d-%d",&year_t,&month_t,&day_t,&number);
	//printf("%d,%d,%d,%05d\n",year_t,month_t,day_t,number);
	// 当前的数据，继续沿用；否则，从1开始写
	if (year == year_t && month == month_t && day == day_t) {
		str.Format("%05d", number+1);
		((CComboBox*)GetDlgItem(IDC_EDIT_SNID))->SetWindowTextA(str);
		str.Format("流水号已更新到：%05d。", number);
	}
	else {
		str.Format("%05d", 1);
		((CComboBox*)GetDlgItem(IDC_EDIT_SNID))->SetWindowTextA(str);
		str.Format("流水号今天还没有更新过。");
	}
	GetDlgItem(IDC_STA_Warn)->SetWindowTextA(str);
	return TRUE;
}

void ConfigSN::OnBnClickedButWrtSn()
{
	// write operation
	if (start_flag) {
		// 获取SN码
		CString cstr_y,cstr_m,cstr_d,cstr_n;
		((CComboBox*)GetDlgItem(IDC_EDIT_Year))->GetWindowTextA(cstr_y);
		((CComboBox*)GetDlgItem(IDC_EDIT_Month))->GetWindowTextA(cstr_m);
		((CComboBox*)GetDlgItem(IDC_EDIT_Day))->GetWindowTextA(cstr_d);
		((CComboBox*)GetDlgItem(IDC_EDIT_SNID))->GetWindowTextA(cstr_n);
		year = _ttoi(cstr_y);
		month = _ttoi(cstr_m);
		day = _ttoi(cstr_d);
		number = _ttoi(cstr_n);

		// 反写回去，便于查看
		cstr_y.Format("%02d",year);
		cstr_m.Format("%02d",month);
		cstr_d.Format("%02d",day);
		cstr_n.Format("%05d",number);
		((CComboBox*)GetDlgItem(IDC_EDIT_Year))->SetWindowTextA(cstr_y);
		((CComboBox*)GetDlgItem(IDC_EDIT_Month))->SetWindowTextA(cstr_m);
		((CComboBox*)GetDlgItem(IDC_EDIT_Day))->SetWindowTextA(cstr_d);
		((CComboBox*)GetDlgItem(IDC_EDIT_SNID))->SetWindowTextA(cstr_n);

		cstr_y.ReleaseBuffer();
		cstr_m.ReleaseBuffer(); 
		cstr_d.ReleaseBuffer();
		cstr_n.ReleaseBuffer();

		if (number == 0) {
			GetDlgItem(IDC_STA_Warn)->SetWindowTextA("流水号不能为0.");
			return;
		}
		CString sn;
		sn.Format("%02d%02d%02d%05d",year,month,day,number);

		// <ZKHYSET*DEVICESN:SN>
		char writeCmd[50] = {0};
		sprintf(writeCmd,"<ZKHYSET*DEVICESN:%s>\r\n",sn.GetBuffer(0));
		sn.ReleaseBuffer();

		//printf("writecmd:%s\n",writeCmd);
		mCommUtils.SendData(writeCmd, (int)(strlen(writeCmd)));
		GetDlgItem(IDC_STA_Warn)->SetWindowTextA("已发送写指令。");
	}
}

void ConfigSN::OnBnClickedButReadSn()
{
	// TODO: 在此添加控件通知处理程序代码
	if (start_flag) {
		// <ZKHYCHK*DEVICESN>
		char ReadCmd[50] = "<ZKHYCHK*DEVICESN>\r\n";
		//printf("ReadCmd:%s\n", ReadCmd);
		GetDlgItem(IDC_STA_Warn)->SetWindowTextA("已发送读指令。");
		mCommUtils.SendData(ReadCmd, (int)(strlen(ReadCmd)));
	}
}

void ConfigSN::OnDeltaposSpinYear(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	CString cstr;
	((CComboBox*)GetDlgItem(IDC_EDIT_Year))->GetWindowTextA(cstr);
	year = _ttoi(cstr);
	cstr.ReleaseBuffer();

	if (pNMUpDown->iDelta < 0) {	// +
		year++;
		if (year >= 99) year = 99;
	}
	else {							// -
		year--;
		if (year >= 99) year = 99;
		else if (year <= 0) year = 0;
	}
	m_editNum.Format("%02d", year);
	((CComboBox*)GetDlgItem(IDC_EDIT_Year))->SetWindowTextA(m_editNum);
	m_editNum.ReleaseBuffer();

	*pResult = 0;
}


void ConfigSN::OnDeltaposSpinMonth(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	CString cstr;
	((CComboBox*)GetDlgItem(IDC_EDIT_Month))->GetWindowTextA(cstr);
	month = _ttoi(cstr);
	cstr.ReleaseBuffer();

	if (pNMUpDown->iDelta < 0) {	// +
		month++;
		if (month >= 12) month = 12;
	}
	else {							// -
		month--;
		if (month >= 12) month = 12;
		else if (month <= 1) month = 1;
	}
	m_editNum.Format("%02d", month);
	((CComboBox*)GetDlgItem(IDC_EDIT_Month))->SetWindowTextA(m_editNum);
	m_editNum.ReleaseBuffer();

	*pResult = 0;
}


void ConfigSN::OnDeltaposSpinDay(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString m_editNum("");
	CString cstr;
	((CComboBox*)GetDlgItem(IDC_EDIT_Day))->GetWindowTextA(cstr);
	day = _ttoi(cstr);
	cstr.ReleaseBuffer();

	if (pNMUpDown->iDelta < 0) {	// +
		day++;
		if (month == 2) {
			if (day >= 29) day = 29;
		}
		else if (month == 4 && month == 6 && month == 9 && month == 11) {	// 1 3 5 7 8 10 12
			if (day >= 30) day = 30;
		}
		else {
			if (day >= 31) day = 31;
		}
	}
	else {							// -
		day--;
		if (month == 2) {
			if (day >= 29) day = 29;
		}
		else if (month == 4 && month == 6 && month == 9 && month == 11) {	// 1 3 5 7 8 10 12
			if (day >= 30) day = 30;
		}
		else {
			if (day >= 31)
				day = 31;
		}

		if (day <= 0) day = 0;
	}
	m_editNum.Format("%02d", day);
	((CComboBox*)GetDlgItem(IDC_EDIT_Day))->SetWindowTextA(m_editNum);
	m_editNum.ReleaseBuffer();

	*pResult = 0;
}


void ConfigSN::OnDeltaposSpinSnid(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMUPDOWN pNMUpDown = reinterpret_cast<LPNMUPDOWN>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	CString cstr;
	((CComboBox*)GetDlgItem(IDC_EDIT_SNID))->GetWindowTextA(cstr);
	number = _ttoi(cstr);
	cstr.ReleaseBuffer();

	CString m_editNum("");
	if (pNMUpDown->iDelta < 0) {	// +
		number++;
		if (number >= 99999) number = 99999;
	}
	else {							// -
		number--;
		if (number >= 99999) number = 99999;
		else if (number <= 1) number = 1;
	}
	m_editNum.Format("%05d", number);
	((CComboBox*)GetDlgItem(IDC_EDIT_SNID))->SetWindowTextA(m_editNum);
	m_editNum.ReleaseBuffer();

	*pResult = 0;
}

int ConfigSN::SeekSerial()
{
	((CComboBox*)GetDlgItem(IDC_COM))->ResetContent();
	HKEY hKey;
	if (::RegOpenKeyEx(HKEY_LOCAL_MACHINE,
		_T("HARDWARE\\DEVICEMAP\\SERIALCOMM"),	// HKEY_LOCAL_MACHINE\HARDWARE\DEVICEMAP\SERIALCOMM
		NULL,
		KEY_READ,
		&hKey) == ERROR_SUCCESS)				// 打开串口注册表对应的键值 
	{
		int i = 0;
		TCHAR portName[256], commName[256];
		DWORD dwLong, dwSize;
		while (1)
		{
			dwLong = dwSize = sizeof(portName);
			if (::RegEnumValue(hKey,
				i,
				portName,
				&dwLong,
				NULL,
				NULL,
				(PUCHAR)commName,
				&dwSize) == ERROR_NO_MORE_ITEMS)//   枚举串口  
				break;
			printf("serial:%s\n", commName);
			((CComboBox*)GetDlgItem(IDC_COM))->AddString(commName);
			((CComboBox*)GetDlgItem(IDC_COM))->SetCurSel(0);
			i++;
		}
		RegCloseKey(hKey);
	}

	return 0;
}

void ConfigSN::OnBnClickedButOpencom()
{
	if (start_flag == true) {
		start_flag = false;
		mCommUtils.CloseCom();

		//TerminateThread(thread_r, 0);
		CloseHandle(thread_r);
		GetDlgItem(IDC_BUT_OpenCom)->SetWindowTextA(_T("打开"));
		GetDlgItem(IDC_BUT_Wrt_SN)->EnableWindow(FALSE);
		GetDlgItem(IDC_BUT_READ_SN)->EnableWindow(FALSE);
		printf("串口关闭.\n");
		return;
	}
	else {
		// 重新打开，先清空数据
		int nIndex = ((CComboBox*)GetDlgItem(IDC_COM))->GetCurSel();
		CString strCBText;
		((CComboBox*)GetDlgItem(IDC_COM))->GetLBText(nIndex, strCBText);
		char* com = strCBText.GetBuffer(0);
		strCBText.ReleaseBuffer();

		char* pos = com + 3;
		int comId = atoi(pos);

		//printf("%d\n", comId);
		if (mCommUtils.OpenSerialPort(comId, SERIAL_BAUD_230400, 8, 1) == false) {
			printf("open serial failed.\n");
			printf("串口打开失败。\n");
			return;
		}
		start_flag = true;
		GetDlgItem(IDC_BUT_OpenCom)->SetWindowTextA(_T("关闭"));
		GetDlgItem(IDC_BUT_Wrt_SN)->EnableWindow(TRUE);
		GetDlgItem(IDC_BUT_READ_SN)->EnableWindow(TRUE);
		//启动线程都串口
		thread_r = NULL;

		thread_r = CreateThread(NULL, 1024, Thread_Serial_r, this, 0, 0);
		printf("串口打开\n");
	}
}


DWORD WINAPI Thread_Serial_r(LPVOID lpParam)
{
	ConfigSN* pLogic = (ConfigSN*)lpParam;
	pLogic->Thread_Serial_Recv();

	return 0;
}
void ConfigSN::Thread_Serial_Recv()
{
	unsigned char data[512] = { 0 };
	unsigned char data_t[512] = { 0 };
	memset(data, 0, sizeof(data));
	memset(data_t, 0, sizeof(data_t));
	//清空串口
	PurgeComm(mCommUtils.m_hComm, PURGE_RXCLEAR | PURGE_TXCLEAR);
	DWORD len = 0;
	//printf("Thread_Serial\n");
	int tt = 0, len_t = 0;
	unsigned char tmp[100] = { 0 };
	char* strx = NULL;
	while (1)
	{
		memset(data, 0, sizeof(data));
		memset(data_t, 0, sizeof(data_t));
		if (mCommUtils.m_hComm == INVALID_HANDLE_VALUE) {
			printf("recv thread failed.\n");
			break;
		}
		//Sleep(1);
		BOOL bReadOK = ReadFile(mCommUtils.m_hComm, data_t, 512, &len, NULL);
		if (bReadOK && (len > 0)) {
			//printf("%s\n", data_t);
			if (data_t[len - 1] != '\n') {
				strcat((char*)tmp, (char*)data_t);
				continue;
			}
			else {
				strcat((char*)tmp, (char*)data_t);
				memcpy(data, tmp, strlen((char*)tmp));
			}
			//清空串口
			PurgeComm(mCommUtils.m_hComm, PURGE_RXCLEAR | PURGE_TXCLEAR);

			printf("%s", data);
			memset(tmp, 0, sizeof(tmp));
			/*
			串口升级过程中有2中模式：指令模式和用户升级模式
			在指令模式下：仅接收指令
			在用户升级模式下：仅接收升级包
			*/
			if (strstr((char*)data, "Success")) {	// 发送下一包
				memset(data, 0, sizeof(data));
				// 写版本信息
				WriteSn();
				GetDlgItem(IDC_STA_Warn)->SetWindowTextA("已存储SN号。");
			}
			else if (strx = (strstr((char*)data, "SN:"))) {
				char sn[20] = { 0 },i = 0;

				for (i = 0; i < strlen(strx+3);i++) {
					if (strx[i+3] != '\r') {
						sn[i] = strx[i + 3];
					}
				}
				sn[i] = '\0';
				printf("sn:%s\n",sn);
				CString SN("");
				SN.Format("SN: %s",sn);
				GetDlgItem(IDC_STA_Dis_SN)->SetWindowTextA(SN);
				SN.ReleaseBuffer();
				GetDlgItem(IDC_STA_Warn)->SetWindowTextA("已接收到SN号。");
			}
		}
	}
	cout << "读取串口数据:结束" << endl;

}

void ConfigSN::WriteSn()
{
	char writeBuf[20] = { 0 };
	sprintf(writeBuf, "%d-%d-%d-%d", year, month, day, number);
	FILE* fpw = fopen("./sn.dat", "wb+");
	fwrite(writeBuf, 1, strlen(writeBuf), fpw);
	fclose(fpw);
}


