﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 USARTUpgrade.rc 使用
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_USART_UPGRADE_DIALOG        102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG1                     130
#define IDD_DIALOG2                     131
#define IDD_DIALOG3                     137
#define IDC_TAB1                        1000
#define IDC_BUTTON1                     1003
#define IDC_BUT_OpenSerial              1003
#define IDC_BUT_Wrt_SN                  1003
#define IDC_BUTTON2                     1004
#define IDC_BUT_SerialDown              1004
#define IDC_BUT_READ_SN                 1004
#define IDC_MFCEDITBROWSE1              1005
#define IDC_BUT_OpenCom                 1005
#define IDC_EDIT1                       1007
#define IDC_Encode_btn                  1008
#define IDC_COMBO_1                     1055
#define IDC_SPIN_1                      1056
#define IDC_EDIT_1                      1057
#define IDC_SPIN_2                      1058
#define IDC_MFCEDITBROWSE2              1058
#define IDC_EDIT_2                      1059
#define IDC_COMBO1                      1059
#define IDC_SPIN_3                      1060
#define IDC_LIST1                       1060
#define IDC_EDIT_3                      1061
#define IDC_LIST2                       1061
#define IDC_SPIN_6                      1061
#define IDC_SPIN_4                      1062
#define IDC_EDIT_6                      1062
#define IDC_EDIT_4                      1063
#define IDC_SPIN_7                      1063
#define IDC_SPIN_5                      1064
#define IDC_EDIT_7                      1064
#define IDC_EDIT_5                      1065
#define IDC_SPIN_8                      1065
#define IDC_COMBO_2                     1066
#define IDC_EDIT_8                      1066
#define IDC_COMBO_3                     1067
#define IDC_SPIN_9                      1067
#define IDC_STA_VER                     1068
#define IDC_EDIT_9                      1068
#define IDC_STAT_INFO                   1069
#define IDC_COMBO_5                     1069
#define IDC_PROGRESS1                   1070
#define IDC_SPIN_11                     1070
#define IDC_COMBO_6                     1071
#define IDC_EDIT_11                     1071
#define IDC_COMBO_4                     1072
#define IDC_EDIT_Year                   1073
#define IDC_SPIN_Year                   1074
#define IDC_SPIN_Month                  1075
#define IDC_EDIT_Month                  1076
#define IDC_SPIN_Day                    1077
#define IDC_EDIT_Day                    1078
#define IDC_SPIN_SNID                   1079
#define IDC_EDIT_SNID                   1080
#define IDC_STA_Dis_SN                  1081
#define IDC_COM                         1082
#define IDC_STA_Warn                    1083
#define IDC_BUT_SET_VER                 1084
#define IDC_BUT_READ_VER                1085
#define IDC_STA_READ_VER                1086
#define IDC_COMBO_7                     1087
#define IDC_EDIT_10                     1088
#define IDC_SPIN_10                     1089
#define IDC_Fresh_COM                   1090
#define IDC_COMBO2                      1091
#define IDC_Bin_SWH                     1091

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1092
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
