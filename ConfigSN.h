﻿#pragma once


// ConfigSN 对话框

#include "CSerial.h"
class ConfigSN : public CDialogEx
{
	DECLARE_DYNAMIC(ConfigSN)

public:
	ConfigSN(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~ConfigSN();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG3 };
#endif

private:
	void InitControlFontSize();				// 初始化控件字体
	bool MySetFontSize(int ID, float times);// 设置自己的字体 
	bool FillInData();
	int SeekSerial();
	void WriteSn();

private:
	int year, month, day, number;
	bool start_flag;	// true-isRUn;false-isClose
	HANDLE thread_r;	// recv data
	CSerial mCommUtils;
	
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButWrtSn();
	afx_msg void OnDeltaposSpinYear(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpinMonth(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpinDay(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpinSnid(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedButOpencom();
	void Thread_Serial_Recv();
	afx_msg void OnBnClickedButReadSn();
};
