# UpgradeTool_AEBS

控制器串口升级工具。

IDE：Visual Studio 2019

配置属性：
Windows SDK版本：10.0
平台工具集：	Visual Studio 2019 (v142)
MFC的使用：		使用标准Windows库
字符集：		使用多字节字符集

加密操作
 对hex加密时，生成的bin文件大小上限为200KB；
 将bin加密时，生成的bin文件大小没有设限。