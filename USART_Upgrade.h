﻿
// USART_Upgrade.h: PROJECT_NAME 应用程序的主头文件
//

#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含 'pch.h' 以生成 PCH"
#endif

#include "resource.h"		// 主符号
#define SERIAL_BAUD_230400			230400 // 115200		// 要与OTA兼容(USART0),若不兼容，可以设为115200
#define SERIAL_BAUD_115200			115200

// CUSARTUpgradeApp:
// 有关此类的实现，请参阅 USART_Upgrade.cpp
//

class CUSARTUpgradeApp : public CWinApp
{
public:
	CUSARTUpgradeApp();

// 重写
public:
	virtual BOOL InitInstance();

// 实现

	DECLARE_MESSAGE_MAP()
};

extern CUSARTUpgradeApp theApp;
