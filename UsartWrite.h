﻿#pragma once

#define DATA_LEN		(1024*500)
#define PKG_SUB_SIZE		4101 // 4096+5
#define PKG_DATA_SIZE		4096
#define PKG_SCREEN_SIZE		2048
#define DATA_LEN_50			50

#define DATA_LEN_4096		4096	
#define DATA_LEN_2048		2048		
#define DATA_LEN_1024		1024		
#define DATA_LEN_512		512		

#include <iostream>
#include <queue>
#include "ControlCAN.h"
using namespace std;
// UsartWrite 对话框
typedef enum {
	No_Status,
	SEND_CMD,				// 串口发送指令
	SEND_DATA,				// 串口发送数据
	SEND_CAN_START_FRM,		// CAN发送开始帧
	SEND_CAN_DATA_FRM,		// CAN发送数据帧
	SEND_CAN_END_FRM,		// CAN发送结束帧
}MODE;


typedef struct _SendData_Info
{
	unsigned char* data;
	unsigned char* sub_data;
	char			version[DATA_LEN_50];
	char			cmd[DATA_LEN_50];
	size_t			len;
	int				time;
	volatile int	mode;
	int				recv_status;		// 0-默认，1-OK,2-ERR
	int				send_status;		// 0-默认，1-继续，2stop
	int				send_cnt;
	volatile int	times;
	volatile int	remain;
	volatile int	checkout;
	int				wait_time;			// 等待时间
	int				ver_Compatibility_flag;	// 版本兼容标志位;0新版本；1旧版本
	volatile int	ver_sw_flag;
	int				bin_type;

	bool			is_dwnloading;		// true下载中，false无下载
}SendData_Info;

typedef struct _Serial_Data {
	char recv_data[DATA_LEN_1024];
	char dispose_data[DATA_LEN_4096];
	char buff_data[DATA_LEN_4096];
	DWORD recv_len;
}Serial_Data;

/*************** 小屏幕升级 *********************/
// CAN升级小屏幕帧ID
enum {
	SCREEN_SUB_START_FRAME_ID = 0x7F0,
	SCREEN_SUB_DWN_FRAME_ID = 0x7F1,
	SCREEN_SUB_END_FRAME_ID = 0x7F2,
	SCREEN_REPLY_FRAME_ID = 0x7F3,
};

/* 工作模式枚举 */
typedef enum _WorkMode
{
	Normal = 0,
	OnlyListen
}WorkMode;
/* 帧格式枚举 */
typedef enum _FrameFormat
{
	DataFrame = 0,		// 数据帧
	RemoteFrame			// 远程帧
}FrameFormat;
/* 帧类型枚举 */
typedef enum _FrameType
{
	StandardFrame = 0,	// 标准帧
	ExternalFrame		// 扩展帧
}FrameType;

/* 发送类型枚举 */
typedef enum _SendType
{
	NormalSend = 0,			// 正常发送
	SingleSend,				// 单次发送
	SelfSendSelfRecv,		// 自收自发
	SingleSelfSendSelfRecv,	// 单次自收自发
}SendType;

typedef struct _Screen_CAN_Upgrade_Str
{
	UINT8 pkg_num;				// 分包编号
	int pkg_len;				// 分包长度
	UINT8 pkg_total_times;		// 总需要发送多少次包
	UINT8 which_operation;		// 反馈哪个操作，1：开始下发帧；2：结束下发帧
	UINT8 screen_status;		// 小屏幕状态 【0：正常】【1：需等待】【2：错误-重发】
}Screen_CAN_Upgrade_Str;

class UsartWrite : public CDialogEx
{
	DECLARE_DYNAMIC(UsartWrite)

public:
	UsartWrite(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~UsartWrite();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG2 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	// 串口处理
	static UINT Thread_Serial_Recv(void* pParam);
	static UINT Thread_Serial_Send(void* pParam);
	void Analysis_Recv_Data(char* data);

	// CAN 处理
	static UINT Thread_Can_Recv(void* param);
	static UINT Thread_Can_Process(void* param);
	static UINT Thread_Can_Upgrade_Logic(void* param);
	bool m_thread_recv_is_stop;
	bool m_thread_process_is_stop;
	bool m_thread_send_pkg_is_stop;

	queue<VCI_CAN_OBJ> m_canInfo_q;
	Screen_CAN_Upgrade_Str m_screen_upgrade_info;
	VCI_CAN_OBJ m_can_send_pbj;			// 发送数据
	bool CANSendData(UINT FrameID, BYTE DataLen, BYTE* Data, FrameFormat FrameFmt, FrameType FrameTp, SendType SendTp);

	CString m_fileName;
	// 按钮
	CButton m_btn_serial_sw_handle;
	CButton m_btn_dwn_sw_handle;
	CButton m_btn_get_version_handle;
	// 升级处理标志
private:
	void ToShowInfo(char* data);
	int SeekSerial();
	void initControlFontSize();				//初始化控件字体
	void initDlgContent();
	bool mySetFontSize(int ID, float times);//设置自己的字体 
	void Init_CAN_Module();
private:
	//bool start_flag;				// 打开串口状态
	bool m_serial_btn_sw;			// 打开串口状态 true打开；false关闭
	bool m_dwn_btn_sw;				// 下载按钮状态 true打开；false关闭
	//volatile HANDLE m_hThread_recv;	// recv data
	//volatile HANDLE m_hThread_send;	// send data
	//bool stopped;					// 接收线程运行状态
	bool thread_tx_stopped;			// 发送线程启动开关；false启动，true关闭
	bool thread_rx_stopped;			// 接收线程启动开关；false启动，true关闭
	CMFCEditBrowseCtrl m_EditBrowse;	
	SendData_Info g_send_data_info;	// 发送数据信息
	CProgressCtrl* prog;			// 进度条
	int cnt1,cnt2,cnt3,cnt4,cnt5;
	char *readVersion;
	int comId;						// 串口ID
	string run_ver, up_ver;			// 运行版本和升级版本号
public:
	//HANDLE m_hComm;
	//int listbox_cnt;
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedButOpenserial();
	afx_msg void OnCbnSelchangeCombo1();
	afx_msg void OnBnClickedButSerialdown();
	afx_msg void OnBnClickedButSetVer();
	afx_msg void OnBnClickedButReadVer();
	afx_msg void OnDeltaposSpin10(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin6(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin7(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin8(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDeltaposSpin9(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedFreshCom();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	afx_msg void OnEnChangeMfceditbrowse2();
};
