#pragma once
class CommUtils
{
public:
    bool ReadCom(unsigned char* ReceiveData, DWORD& ReceiveLength);
    void CloseCom();
    bool WriteCom(unsigned char* sendchar, int sendsize);
    bool OpenCom(int Port);

    CommUtils();
    virtual ~CommUtils();
    int m_Port;
    char szCurPath[256];
    bool bOpenCom;

private:
    OVERLAPPED ReadovReady, WriteovReady;
    HANDLE hComm;
    
};

