﻿
// USART_UpgradeDlg.h: 头文件
//

#pragma once
#include "Encode.h"
#include "UsartWrite.h"
#include "ConfigSN.h"
// CUSARTUpgradeDlg 对话框 public CDialog, 
#include "configure.h"
class CUSARTUpgradeDlg :public CDialogEx
{
// 构造
public:
	CUSARTUpgradeDlg(CWnd* pParent = nullptr);	// 标准构造函数
	virtual ~CUSARTUpgradeDlg();
// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_USART_UPGRADE_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

private:
	void OnTabCtrInit();
	void OnTabCtlSelect();
	void initControlFontSize();				//初始化控件字体
	bool mySetFontSize(int ID, float times);//设置自己的字体 

private:
	CTabCtrl	m_tab;
#ifdef ONE_DIALOG
	UsartWrite* m_page2;
#else
#ifdef TWO_DIALOG
	UsartWrite* m_page2;
	Encode* m_page1;
#else
	Encode		*m_page1;
	UsartWrite	*m_page2;
	ConfigSN	*m_page3;
#endif
#endif
// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	
public:
	afx_msg void OnTcnSelchangeTab1(NMHDR* pNMHDR, LRESULT* pResult);
};
